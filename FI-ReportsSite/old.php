<?php 
include("auth.php");
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Dashboard - Beta</title>

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>



	<style type="text/css">
	html, body{
		height: 100%;
</style>

</head>
<body>
<ul style="margin-top: 2%; margin-left: 2%" class="nav nav-pills">
  <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
  <li class="nav-item"><a class="nav-link active" href="#">Reports</a></li>
    <li style="margin-left: 15px;" class="nav-item"><a style="color:white" class="nav-link bg-danger" href="logout.php">Logout</a></li>
</ul>
<br>
<div class="container">
<h3>Reports:</h3>
<hr>
<div class="accordion" id="accordionExample">
		<?php 	
	require('db.php');
$i = 0;
$sql = "SELECT `report` FROM `automation_reports` GROUP BY `reportid`;";
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query))
{
    foreach($row as $key => $value)
    {

    	?>
  <div class="card">
    <div class="card-header" id="heading<?php echo $i ?>">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>">
          Report #1: 8/6/2018
        </button>
      </h5>
    </div>

    <div id="collapse<?php echo $i ?>" class="collapse" aria-labelledby="heading<?php echo $i ?>" data-parent="#accordionExample">
      <div style="text-align: center;" class="card-body">
      	    	<h3 style="float: left;"> Rating-Pull: </h3>
      	    		<?php 
      	    		    	$i++;
        echo "$value";
        ?>
</div>
</div>
</div>
<?php
    }
}
?>
</div>
</body>
</html>
