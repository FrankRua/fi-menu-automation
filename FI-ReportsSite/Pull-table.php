<?php 
include("auth.php");
require('db.php');
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Data - Beta </title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>			</head>
                <ul style="margin-top: 2%; margin-left: 2%" class="nav nav-pills">
            <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="dashboard.php">Reports</a></li>
            <li class="nav-item"><a class="nav-link" href="data.php">Report Summary</a></li>
            <li class="nav-item"><a class="nav-link active" href="Pull-table.php">View DB Table </a></li>
            <li style="margin-left: 15px;" class="nav-item"><a style="color:white" class="nav-link bg-danger" href="logout.php">Logout</a></li>
        </ul>
        <br> 
<div class="table-container" style="width:70%; margin: 0 auto;">
					<?php 
					$sql = "SELECT * FROM `automation_reports`;";
					$table = "";
					$result = mysqli_query($connection, $sql) or die(mysql_error());

					$table = "<table class='table table-hover table-bordered'>";
					$table .= "<thread>";
					$table .= "<tr>";
					$fieldsInfo = $result->fetch_fields();

					foreach($fieldsInfo as $fieldinfo)
						$table .= "<th scope='col'>{$fieldinfo->name}</th>";
					$table .= "</tr>";
					$table .= "</thead>";
					$table .= "<tbody>";


					while ($row = $result->fetch_assoc()) {
						$table .= "<tr>";
						foreach ($row as $columnValue) {
							$table .= "<td>$columnValue</td>";

						}
						$table .= "</tr>";
					}
					$table .= "</tbody>";
					$table .= "</table>";
					echo $table;
					?>
</div>
</html>     