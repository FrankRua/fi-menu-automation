<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login - Beta</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>
    <script type="text/javascript">
        function ChangeDisplaySuccess() {
            document.getElementById('a_login').style.display = 'block';
        }

        function ChangeDisplayDanger() {
            document.getElementById('a_login_two').style.display = 'block';
        }
    </script>
</head>
<body style="   background-color: #34495e;" >
        <form action=""  method="POST" name="Login_Form" class="form-signin">
            <h3 class="form-signin-heading">OTI Automation Reports</h3>
            <hr class="colorgraph">
            <br>
            <!-- Display Message -->
            <div style="display: none;" id="a_login" class="alert alert-success" role="alert">Login Successful - Redirecting... </div>
            <div style="display: none;" id="a_login_two" class="alert alert-danger" role="alert">Username or password incorrect. </div>
            <!-- /END/ Display Message -->
            <input type="text" class="form-control" name="Username" placeholder="Username" value="" required="" autofocus="" />
            <br>
            <input type="password" class="form-control" name="Password" placeholder="Password" value="" required="" />
            <button id="login_button" class="btn btn-lg  btn-block" name="Submit" value="Login" type="Submit">Login</button>
        </form>

</body>

</html>
<?php
require('db.php');
session_start();
if(isset($_POST['Submit'])){

	$username_post = $_POST['Username'];
	$password_post = $_POST['Password'];

	$sql = "SELECT * FROM `users` WHERE email='$username_post' AND password='$password_post';";

	$result = mysqli_query($connection, $sql) or die(mysql_error());
	$rows = mysqli_num_rows($result);

	//Grab all row data, because might as well.
	$single_row = mysqli_fetch_assoc($result); //Grab the login row

	$id = $single_row['id'];
	$email = $single_row['email'];
	$firstname = $single_row['firstname'];
	$lastname = $single_row['lastname'];
	$signup = $single_row['signup_date'];
	$status = $single_row['status'];
	$users_allowed = $single_row['users_allowed'];
	$HWID = $single_row['HWID'];

	if($rows==1){

		$_SESSION['username'] = $username_post; //create session var. 'username' and set it to the email here.
		$_SESSION['firstname'] = $firstname;
		$_SESSION['status'] = $status;
		$_SESSION['signup'] = $signup;
		$_SESSION['email'] = $email;
		echo '<script type="text/javascript">',
		'ChangeDisplaySuccess();',
		'</script>';
		header("Location: home.php");
	}else{
		echo '<script type="text/javascript">',
		'ChangeDisplayDanger();',
		'</script>';
	}
}

?>