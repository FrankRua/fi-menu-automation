<?php 
include("auth.php");
require('db.php');
?>
    <!doctype html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Data - Beta </title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>
        <script type="text/JavaScript" src="js/chart.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <style type="text/css">
                html,
            body {
                height: 95%;
        </style>
    </head>

    <body>
        <ul style="margin-top: 2%; margin-left: 2%" class="nav nav-pills">
            <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
            <li class="nav-item"><a class="nav-link" href="dashboard.php">Reports</a></li>
            <li class="nav-item"><a class="nav-link active" href="dashboard.php">Report Summary</a></li>
            <li class="nav-item"><a class="nav-link" href="Pull-table.php">View DB Table </a></li>
            <li style="margin-left: 15px;" class="nav-item"><a style="color:white" class="nav-link bg-danger" href="logout.php">Logout</a></li>
        </ul>
        <div id="container" style="min-width: 100%; max-width: 800px; height: 95%; margin: 0 auto;"></div>
        
        <!-- Grab values from database and display them in graph -->

   </body>
  	</html>