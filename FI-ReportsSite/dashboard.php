<?php 
include("auth.php");
require('db.php');

$sql = 'SELECT `avatar` FROM `users`;';
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$av = $row['avatar'];
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard - Beta
    </title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous">
    </script>
    <style type="text/css">
      html, body{
       
        }
        
        .av_photo{
border-radius: 100px;
    border: 3px solid lightgrey;
    padding: 5px;
}

.dropleft{
margin-left: 93%;
margin-top: -3.8%;
}

.dropdown-menu{

}


    .dropdown-menu::before {
    position: absolute;
    display: inline-block;
    border-bottom: 7px solid transparent;
    border-top: 7px solid transparent;
    border-left: 7px solid #CCC;
    display: inline-block;
    right: -7px;
    top: 42px;
    content: '';
}

.dropdown-menu::after {
    position: absolute;
    display: inline-block;
    border-bottom: 6px solid white;
    border-top: 6px solid transparent;
    border-left: 6px solid transparent;
    display: inline-block;
    right: -7px;
    top: 42px;
    content: '';
}


#footer {
   position:absolute;
   bottom:0;
   width:100%;
   height:30px;   /* Height of the footer */
   font-size: 14px;
    color: gray;
    padding: 5px;
}

    </style>
  </head>
  <body>
        <ul style="margin-top: 2%; margin-left: 2%" class="nav nav-pills">
            <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
            <li class="nav-item"><a class="nav-link active" href="dashboard.php">Reports</a></li>

            <div class="navbar dropleft">
                <li data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-item">
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> <a class="dropdown-item" href="#">Change Avatar Photo</a> <a class="dropdown-item" href="#">Settings</a> <a class="dropdown-item" href="logout.php">Logout</a> </div>
                    <a href=""><img class="av_photo" src=<?php echo $av ?> alt=""></a>
                </li>
            </div>
        </ul>
    <br>
    <div class="container">
      <h3>Reports:
      </h3>
      <hr>
      <div class="accordion" id="accordionExample">
        <?php 	
$i = 0;
$sql = "SELECT `reportid`, `date`, GROUP_CONCAT(`report`) FROM `automation_reports` GROUP BY `reportid`;";
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query))
{
?>
        <div class="card">
          <div class="card-header" id="heading<?php echo $i ?>">
            <h5 class="mb-0">
              <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i ?>" aria-expanded="true" aria-controls="collapse<?php echo $i ?>">
                <?php echo $row['date']; ?>
              </button>
            </h5>
          </div>
          <div id="collapse<?php echo $i ?>" class="collapse" aria-labelledby="heading<?php echo $i ?>" data-parent="#accordionExample">
            <div style="text-align: center;" class="card-body">
              <?php 
echo '<h3 style="text-align:center;"> Rating-Pull: </h3>';
echo $row['GROUP_CONCAT(`report`)'];
$i++;
?>
            </div>
          </div>
        </div>
        <?php
}
?>
      </div>
      </body>
    </html>
