<?php 
include("auth.php");
require('db.php');


$sql = 'SELECT COUNT(*) FROM `automation_reports` WHERE `success` = 0;';
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
 $success = $row['COUNT(*)'] ;
}

?>
    <?php
$sql = 'SELECT COUNT(*) FROM `automation_reports` WHERE `success` = 1;';
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$failed = $row['COUNT(*)'];
}
?>
        <?php
$sql = 'SELECT COUNT(*) FROM `automation_reports`;';
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$total = $row['COUNT(*)'];
}

?>

<?php
$sql = 'SELECT `avatar` FROM `users`;';
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$av = $row['avatar'];
}

?>
            <!doctype html>
            <html lang="en">

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <title>Home</title>
                <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
                <script type="text/JavaScript" src="js/chart.js"></script>
                <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/exporting.js"></script>
                <script src="https://code.highcharts.com/modules/export-data.js"></script>
                <link rel="stylesheet" type="text/css" href="css/home.css">
                <?php
// get the current URL
    $link = $_SERVER['REQUEST_URI'];
?>
            </head>

            <body>
                <ul style="margin-top: 2%; margin-left: 2%" class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link active" href="#">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="dashboard.php">Reports</a></li>
                    <div class="navbar dropleft">
                        <li data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-item">
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> 
                                <a class="dropdown-item" href="logout.php">Change Avatar Photo</a> 
                                <a class="dropdown-item" href="#">Settings</a> 
                                <a class="dropdown-item" href="logout.php">Logout</a> </div>
                            <a href="#"><img class="av_photo" src=<?php echo $av ?>> </a>
                        </li>
                    </div>
                </ul>
                <div class="container">
                    <br>
                    <h3>Quick Overview:</h3>
                    <hr>
                    <div class="card-deck">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Sucessful Reports</h5>
                                <p class="card-text">Reports that were ran where data matches from sent / recieved data.</p>
                                <button id="successtoggle" class="btn btn-success">
                                    <?php echo $success ?>
                                </button>
                            </div>
                        </div>
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Failed Reports</h5>
                                <p class="card-text">Reports that were ran where data does not match from sent / recieved data.</p>
                                <a href="#" id="failtoggle" class="btn btn-danger">
                                    <?php echo $failed ?>
                                </a>
                            </div>
                        </div>
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Total Reports</h5>
                                <p class="card-text">Total number of reports ran between success and fail.</p>
                                <a href="#" id="totaltoggle" class="btn btn-primary">
                                    <?php echo $total ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div style="height:550px" id="container">
                        <script language="JavaScript">
                            // Months for Graph start
                            //Janruary ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
    
$jan = $row['COUNT(*)'];
    
}
?>
                            //Janruary - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 1 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jan_fail = $row['COUNT(*)'];
}
?>
                            //Janruary - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 1 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jan_pass = $row['COUNT(*)'];
}
?>
                            //February ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 2';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$feb = $row['COUNT(*)'];
}
?>
                            //February - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 2 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$feb_fail = $row['COUNT(*)'];
}
?>
                            //February - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 2 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$feb_pass = $row['COUNT(*)'];
}
?>
                            //March ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 3';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$mar = $row['COUNT(*)'];
}
?>
                            //March - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 3 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$mar_fail = $row['COUNT(*)'];
}
?>
                            //March - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 3 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$mar_pass = $row['COUNT(*)'];
}
?>
                            //April ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 4';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$apr = $row['COUNT(*)'];
}
?>
                            //April - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 4 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$apr_fail = $row['COUNT(*)'];
}
?>
                            //April - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 4 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$apr_pass = $row['COUNT(*)'];
}
?>
                            //May ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 5';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$may = $row['COUNT(*)'];
}
?>
                            //May - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 5 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$may_fail = $row['COUNT(*)'];
}
?>
                            //May - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 5 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$may_pass = $row['COUNT(*)'];
}
?>
                            //June ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 6';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jun = $row['COUNT(*)'];
}
?>
                            //June - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 6 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jun_fail = $row['COUNT(*)'];
}
?>
                            //June - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 6 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$june_pass = $row['COUNT(*)'];
}
?>
                            //July ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 7';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jul = $row['COUNT(*)'];
}
?>
                            //July - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 7 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jul_fail = $row['COUNT(*)'];
}
?>
                            //July - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 7 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$jul_pass = $row['COUNT(*)'];
}
?>
                            //August ->
                            //August - Total
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 8';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$aug = $row['COUNT(*)'];
}
?>
                            //August - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 8 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$aug_fail = $row['COUNT(*)'];
}
?>
                            //August - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 8 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$aug_pass = $row['COUNT(*)'];
}
?>
                            //September ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 9';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$sep = $row['COUNT(*)'];
}
?>
                            //September - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 9 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$sep_fail = $row['COUNT(*)'];
}
?>
                            //September - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 9 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$sep_pass = $row['COUNT(*)'];
}
?>
                            //October ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 10';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$oct = $row['COUNT(*)'];
}
?>
                            //September - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 10 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$oct_fail = $row['COUNT(*)'];
}
?>
                            //September - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 10 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$oct_pass = $row['COUNT(*)'];
}
?>
                            //November ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 11';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$nov = $row['COUNT(*)'];
}
?>
                            //Nov - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 11 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$nov_fail = $row['COUNT(*)'];
}
?>
                            //Nov - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 11 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$nov_pass = $row['COUNT(*)'];
}
?>
                            //December ->
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`
WHERE MONTH(date) = 12';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$dec = $row['COUNT(*)'];
}
?>
                            //Dec - Fail
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 12 AND success = 1';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$dec_fail = $row['COUNT(*)'];
}
?>
                            //Dec - Pass
                            <?php 
$sql = 'SELECT
COUNT(*)
FROM
  `automation_reports`

WHERE MONTH(date) = 12 AND success = 0';

$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$dec_pass = $row['COUNT(*)'];
}
?>
                            //close
                            $(document).ready(function () {
                                Highcharts.theme = {
                                    colors: ['#e74c3c', '#2980b9', '#27ae60', '#7798BF', '#aaeeee', '#ff0066'
        , '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee']
                                    , chart: {
                                        backgroundColor: null
                                        , style: {
                                            fontFamily: 'Dosis, sans-serif'
                                        }
                                    }
                                    , title: {
                                        style: {
                                            fontSize: '16px'
                                            , fontWeight: 'bold'
                                            , textTransform: 'uppercase'
                                        }
                                    }
                                    , tooltip: {
                                        borderWidth: 0
                                        , backgroundColor: 'rgba(219,219,216,0.8)'
                                        , shadow: false
                                    }
                                    , legend: {
                                        itemStyle: {
                                            fontWeight: 'bold'
                                            , fontSize: '13px'
                                        }
                                    }
                                    , xAxis: {
                                        gridLineWidth: 1
                                        , labels: {
                                            style: {
                                                fontSize: '12px'
                                            }
                                        }
                                    }
                                    , yAxis: {
                                        minorTickInterval: 'auto'
                                        , title: {
                                            style: {
                                                textTransform: 'uppercase'
                                            }
                                        }
                                        , labels: {
                                            style: {
                                                fontSize: '12px'
                                            }
                                        }
                                    }
                                    , plotOptions: {
                                        candlestick: {
                                            lineColor: '#404048'
                                        }
                                    }, // General
                                    background2: '#F0F0EA'
                                };
                                // Apply the theme
                                Highcharts.setOptions(Highcharts.theme);
                                var chart = Highcharts.chart('container', {
                                        chart: {
                                            type: 'column'
                                        }
                                        , title: {
                                            text: 'Monthy Report Summary'
                                        }
                                        , subtitle: {
                                            text: 'Source: "reports" database'
                                        }
                                        , xAxis: {
                                            categories: [
      'Jan'
      , 'Feb'
      , 'Mar'
      , 'Apr'
      , 'May'
      , 'Jun'
      , 'Jul'
      , 'Aug'
      , 'Sep'
      , 'Oct'
      , 'Nov'
      , 'Dec'
    ]
                                            , crosshair: true
                                        }
                                        , yAxis: {
                                            min: 0
                                            , title: {
                                                text: 'Total Reports'
                                            }
                                        }
                                        , tooltip: {
                                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>'
                                            , pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>'
                                            , footerFormat: '</table>'
                                            , shared: true
                                            , useHTML: true
                                        }
                                        , plotOptions: {
                                            column: {
                                                pointPadding: 0.2
                                                , borderWidth: 0
                                            }
                                        }
                                        , series: [{
                                            name: 'Fail'
                                            , mod: 'f'
                                            , data: [<?php echo $jan_fail ?>, <?php echo $feb_fail ?>, <?php echo $mar_fail ?>, <?php echo $apr_fail ?>, <?php echo $may_fail ?>, <?php echo $jun_fail ?>, <?php echo $jul_fail ?>, <?php echo $aug_fail ?>
           , <?php echo $sep_fail ?>, <?php echo $oct_fail ?>, <?php echo $nov_fail ?>, <?php echo $dec_fail ?>]
  , }, {
                                            name: 'Total'
                                            , mod: 't'
                                            , data: [<?php echo $jan ?>, <?php echo $feb ?>, <?php echo $mar ?>, <?php echo $apr ?>, <?php echo $may ?>, <?php echo $jun ?>, <?php echo $jul ?>, <?php echo $aug ?>, <?php echo $sep ?>
           , <?php echo $oct ?>, <?php echo $nov ?>, <?php echo $dec ?>]
  }, {
                                            name: 'Pass'
                                            , mod: 'p'
                                            , data: [<?php echo $jan_pass ?>, 0, 0, 0, 0, 0, 0, <?php echo $aug_pass ?>, 0, 0, 0, 0]
  , }]
                                    })
                                    //Button Toggles for graph
                                var $button = $('#successtoggle');
                                $button.click(function () {
                                    var series = chart.series[2];
                                    $(this).toggleClass("btn-secondary btn-success");
                                    if (!$(this).hasClass('btn-secondary')) {
                                        series.show();
                                    }
                                    else {
                                        series.hide();
                                    }
                                });
                                var $button = $('#failtoggle');
                                $button.click(function () {
                                    var series = chart.series[0];
                                    $(this).toggleClass("btn-secondary btn-danger");
                                    if (!$(this).hasClass('btn-secondary')) {
                                        series.show();
                                    }
                                    else {
                                        series.hide();
                                    }
                                });
                                var $button = $('#totaltoggle');
                                $button.click(function () {
                                    var series = chart.series[1];
                                    $(this).toggleClass("btn-secondary btn-primary");
                                    if (!$(this).hasClass('btn-secondary')) {
                                        series.show();
                                    }
                                    else {
                                        series.hide();
                                    }
                                });
                                
                                $(function() {
  $('a[href*=#]').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
  });
});

                            });
                        </script>
                    </div>
                    
                </div>
            </body>

            </html>
            <!--         <a href="dashboard.php"><button type="button" name="admin_panel" class="btn btn-primary">Dashboard</button></a>
        <a href="account_settings.php"><button type="button" name="logout" class="btn btn-primary">Account Settings</button></a>
        <a href="logout.php"><button type="button" name="logout" class="btn btn-danger">Logout</button></a> -->
            <!-- 
                <h1 class="display-4">Welcome Back ?php echo $_SESSION['firstname']; </h1> -->