<?php 
include('auth.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Account Settings</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script type="text/javascript">
		function ChangeDisplaySuccess(){
			document.getElementById('form_success').style.display = 'block';
		}

		function ChangeDisplayDanger(){
			document.getElementById('form_error').style.display = 'block';
		}
	</script>

	<style type="text/css">
	#account_form{
		width: 35%;
		border: 1px lightgrey solid;
		padding: 30px;
		border-radius: 10px;
		margin:0 auto;
		margin-top: 5%;
	}    	

</style>
</head>
<body>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="cart.php">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Account Settings</li>
  </ol>
</nav>

	<form method="POST" action="account_settings.php" id="account_form">
		<div class="form-group">
			<label for="email">Email Address</label>
			<input type="email" class="form-control"name="email" aria-describedby="emailHelp" placeholder="Enter email">
			<input type="email" class="form-control" name="email_confirm" aria-describedby="emailHelp" placeholder="E-Mail Confirmation">
			<small id="emailHelp" class="form-text text-muted">Please double-check your e-mail to confirm.</small>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" name="password" placeholder="Password">
			<input type="password" class="form-control" name="password_confirm" placeholder="Password Confirmation">
		</div>
		<div class="form-group">
			<label for="email">Name</label>
			<input type="text" class="form-control" name="first_name" placeholder="First Name">
			<input type="text" class="form-control" name="last_name" placeholder="Last Name">
			<small id="emailHelp" class="form-text text-muted"></small>
		</div>
		<button name="form_submit" type="submit" class="btn btn-primary">Save Changes</button>
		<br>
		<br>
		<div style="display: none;" id="form_success" class="alert alert-success" role="alert">
			Settings successfully updated!
		</div>

		<div style="display: none;" id="form_error" class="alert alert-danger" role="alert">
			Error - Settings have not been updated.
		</div>
	</form>


	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

<?php
require('db.php');


if(isset($_POST['form_submit']))
{

	$email_orginal = $_SESSION['email'];
	$email = $_POST['email'];
	$email_confirm = $_POST['email_confirm'];
	
	$password = $_POST['password'];
	$password_confirm = $_POST['password_confirm'];

	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];

	$i = 0;

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\

	if (empty($first_name) == false) {
		$sql = "UPDATE `users` SET firstname='$first_name' WHERE email='$email_orginal';";

		$result = mysqli_query($connection, $sql) or die(mysql_error());
		echo "catch 4";
	}elseif (empty($last_name) == false) {
		$sql = "UPDATE `users` SET lastname='$last_name' WHERE email='$email_orginal';";

		$result = mysqli_query($connection, $sql) or die(mysql_error());
		echo "catch 5";
	}elseif (empty($email) == false && $email == $email_confirm) {
		$sql = "UPDATE `users` SET email='$email' WHERE email='$email_orginal';";

		$result = mysqli_query($connection, $sql) or die(mysql_error());
		$_SESSION['email'] = $email;
		$email_orginal = $email;
		echo "catch 6";
	}elseif (empty($password) == false && $password == $password_confirm) {
		$sql = "UPDATE `users` SET password='$password' WHERE email='$email_orginal';";

		$result = mysqli_query($connection, $sql) or die(mysql_error());
		echo "catch 7";
	}else{$i = 1;}

	if($i == 0){
		echo '<script type="text/javascript">',
		'ChangeDisplaySuccess();',
		'</script>';
	}else{
		echo '<script type="text/javascript">',
		'ChangeDisplayDanger();',
		'</script>';
	}
}
?>
