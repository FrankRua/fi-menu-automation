<?php 
include("auth.php");
require('db.php');


$sql = 'SELECT `avatar` FROM `users`;';
$query = mysqli_query($connection,  $sql);
while($row = mysqli_fetch_assoc($query)){
$av = $row['avatar'];
}

?>
<!doctype html>
            <html lang="en">

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <title>Home</title>
                <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
                <link rel="stylesheet" type="text/css" href="css/home.css">

            </head>

            <body>
                
<script type="text/javascript">
    function setCookie(name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    }
    
    $(document).ready(function () {
        $(".av").click(function () {
            var input = $(this);
            console.log(input.attr("src"));
            setCookie('avatar_url', input.attr("src"), 7);
        });
    });
</script>
                
         
                <ul style="margin-top: 2%; margin-left: 2%" class="nav nav-pills">
                    <li class="nav-item"><a class="nav-link" href="home.php">Home</a></li>
                    <li class="nav-item"><a class="nav-link" href="dashboard.php">Reports</a></li>
                    <div class="navbar dropleft">
                        <li data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-item">
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"> <a class="dropdown-item" href="logout.php">Change Avatar Photo</a> <a class="dropdown-item" href="#">Settings</a> <a class="dropdown-item" href="logout.php">Logout</a> </div>
                            <a href=""><img class="av_photo" src=<?php echo $av ?>></a>
                        </li>
                    </div>
                </ul>
                <div class="container">
                    <div class="avatardisplay">
                        <a href="changeavatar.php"><img class="av" src="img/avatar/_-firewoman.png"></a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Afro-Man.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Asian-Man.png"></a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Asian-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Basketball-Man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Basketball-Woman.png"></a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__bellboy.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__bellgirl.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Chicken.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Colourful-Man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Cook-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Cook.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Customer-Service-Man.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Customer-Service-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Decorated-man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__decorated-woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__decorated-woman.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Doctor.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Dominatrix.png"> </a>
                            <a href="changeavatar.php"><img class="av" src="img/avatar/__Farmer.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Fireman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Floral-man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Floral-woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Gangster-man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Gangster-woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__gentleman.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Hindu-Man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Hindu-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__hipster-girl.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Hispanic-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Horse.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Jew.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Mechanic-Woman.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Mechanic.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Monk.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Music-Man.png"></a> 
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Music-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Muslim-Man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Muslim-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Nerd-man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Nerd-woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Ninja.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Nun.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Nurse-Woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Nurse.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__photographer.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Pilot.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Police-man.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Police-woman.png"> </a>
                        <a href="changeavatar.php"><img class="av" src="img/avatar/__Priest.png"></a>
                    </div>
                </div>
                
                </body>
</html>