﻿//makecert.exe will retain cert info for HTTPS / Fiddler.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Xml.Linq;
using Fiddler;
using FI_Automation.Properties;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using MainMenu;
using MainMenu.Data;
using MainMenu.Data.DataProviders;
using MainMenu.Data.Utilities;
using FI_Automation.MonthlyLoadDAL;
using MainMenu.Insurance;
using MainMenu.ObjectModel;
using MaterialSkin;
using MaterialSkin.Controls;
using MySql.Data.MySqlClient;
using Ninject;
using OptionSoft.Calculation;
using OptionSoft.Calculation.IInsurance;
using OptionSoft.Core.Net4;
using OptionSoft.Interfaces;
using OptionSoft.Licensing;
using TestStack.White.Configuration;
using TestStack.White.Factory;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.TableItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;
using TestStack.White.WindowsAPI;
using Application = TestStack.White.Application;
using Button = TestStack.White.UIItems.Button;
using CheckBox = TestStack.White.UIItems.CheckBox;
using ComboBox = TestStack.White.UIItems.ListBoxItems.ComboBox;
using Label = TestStack.White.UIItems.Label;
using License = OptionSoft.Licensing.License;
using OtherSetups = MainMenu.Data.DataProviders.OtherSetups;
using Panel = TestStack.White.UIItems.Panel;
using Path = System.IO.Path;
using TextBox = TestStack.White.UIItems.TextBox;

namespace FI_Automation
{
    public partial class Main : MaterialForm
    {
        public enum ResponseType
        {
            requestOnly = 0,
            responseOnly = 1,
            both = 2
        }

        public enum RatingType
        {
            VSC = 0,
            TireAndWheel = 1,
            Chemical = 2,
            PPM = 3,
            Roadside = 4,
            GAP = 5
        }

        private const string connectionString =
            "datasource=127.0.0.1;port=3306;username=root;password=;database=reports;SslMode=none;";

        private const string Separator = ""; // For Serparting Requests
        private static readonly List<Tuple<string, int>> reportList = new List<Tuple<string, int>>();
        private static string XMLRequest; // Rating XML from us.
        private readonly XmlValues GetValues = new XmlValues(); // Grab XML Values for rating company

        private readonly List<string>
            OriginalValues = new List<string>(); // Original values for comparision to XML data.

        private readonly List<string> SentData = new List<string>(); // What the menu sent.
        private Application _application;
        private Window _mainWindow; // Get the Main window.
        private string filepath = string.Empty; // For current file path.
        private Form2 form2 = new Form2(); // XML Data View
        private int id; // Menu Process ID
        private bool loaded; // Is the menu loaded?
        private string menu_title = string.Empty; // Because each menu title is the dealers name.
        private bool responseNeeded;
        private string responseString = string.Empty; // XML Response
        private readonly List<string> rateValues = new List<string>(); // Rate table values.
        private string selectedRate = string.Empty; // Selected from table.
        private string DealType = string.Empty; // Deal Type handled by _mainWindow
        public Loading loader = new Loading();

        public Main()
        {
            InitializeComponent();
            CreateObjectGraph();
            loader.Show();
            SetupAutomation(); // Hooks process for automation.
            CaptureConfiguration = new UrlCaptureConfiguration(); // Configure Fiddler.
            SettingsState();
        }

        private UrlCaptureConfiguration CaptureConfiguration { get; }

        /// <summary>
        ///     Removes spaces from XML files.
        /// </summary>
        /// <param name="strFile">Path to file.</param>
        public static void DoRemovespace(string strFile)
        {
            var str = File.ReadAllText(strFile);
            str = str.Replace("\n", string.Empty);
            str = str.Replace("\r", string.Empty);
            var regex = new Regex(@">\s*<");
            var cleanedXml = regex.Replace(str, "><");
            File.WriteAllText(strFile, cleanedXml);
        }

        public static bool InstallCertificate()
        {
            if (CertMaker.rootCertExists()) return true;
            if (!CertMaker.createRootCert()) return false;

            if (!CertMaker.trustRootCert()) return false;

            return true;
        }

        public static bool UninstallCertificate()
        {
            if (CertMaker.rootCertExists())
                if (!CertMaker.removeFiddlerGeneratedCerts(true))
                    return false;

            return true;
        }

        /// <summary>
        ///     Formats items in both Orginal and Sent data list so that things like rounding will be the same.
        /// </summary>
        public void FormatAndGrabItems()
        {
            // Since data formatting is different between the values in the menu vs what is actually formatted and sent in the XML.
            // Some things have to be formatted so that when I go to compare the items, one thing is 6.000 and another is 6.0%
            PullOriginalValues();
            var data = ProcessXml(Environment.CurrentDirectory + @"\request.xml", GetValues.Premier).Trim(',');
            foreach (var s in data.Split(',')) SentData.Add(s);

            SentData[0] = SentData[0].Split(new[] {".xml"}, StringSplitOptions.None)[1];
            OriginalValues[0] =
                SentData
                    [0]; // This is because Miliseconds are always going to be off from the two so they're never going to match.

            SentData[1] = Math.Round(Convert.ToDouble(SentData[1]), 2).ToString(); // 901.97901454 -> 901.92

            OriginalValues[1] =
                Math.Abs(Convert.ToDouble(OriginalValues[1].Replace('$', ' ')))
                    .ToString(); // $901.92 -> 901.92 <BasePayAmount>

            OriginalValues[2] =
                Math.Round(Convert.ToDouble(OriginalValues[2].Replace('%', ' ')), 2).ToString(); // 6.4000% -> 6.4

            OriginalValues[3] =
                Math.Round(Convert.ToDouble(OriginalValues[3].Replace('$', ' ')), 2).ToString(); // $0.00 -> 0.00

            SentData[9] = Math.Round(Convert.ToDouble(SentData[9]), 2).ToString(); // 901.917245 -> 901.92
            OriginalValues[9] =
                Math.Abs(Convert.ToDouble(OriginalValues[9].Replace('$', ' '))).ToString(); // $901.92 -> 901.92

            SentData[10] = SentData[10].Replace('$', ' '); // $10.00 -> 10.00
            OriginalValues[19] = OriginalValues[19].Split(' ')[0]; // Get Provider

            // Because Cash deals pull from the hidden values for [5] (Days To Pay) I just set it as N/A because days to pay is 0.
            if (DealType == "Cash  ") SentData[5] = "N/A";
        }

        /// <summary>
        ///     Populates the XML comparision table for sent / recieved values.
        /// </summary>
        public void PopulateCompareView()
        {
            if (form2.IsDisposed) form2 = new Form2();
            form2.Show(this);

            FormatAndGrabItems();

            foreach (var s in OriginalValues) form2.listView1.Items.Add(s);

            try
            {
                for (var i = 0; i < GetValues.Premier.Count(); i++)
                    form2.listView1.Items[i].Text =
                        "<" + GetValues.Premier[i] + ">" + form2.listView1.Items[i].Text.Trim();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) MessageBox.Show(ex.ToString(), ex.InnerException.Message);
            }

            foreach (var s in SentData) form2.listView2.Items.Add(s);

            try
            {
                for (var i = 0; i < GetValues.Premier.Count(); i++)
                    form2.listView2.Items[i].Text =
                        "<" + GetValues.Premier[i] + ">" + form2.listView2.Items[i].Text.Trim();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) MessageBox.Show(ex.ToString(), ex.InnerException.Message);
            }
        }

        /// <summary>
        ///     Pulls items and fields from Main Menu before rating.
        /// </summary>
        public void PullOriginalValues()
        {
            var terms = new List<string>();
            var FinalPay = string.Empty;
            var termText = string.Empty;
            OriginalValues.Add(DateTime.Now.Date.ToString("yyyy-MM-dd") + "T" +
                               DateTime.Now.ToString("HH:mm:ss" + DateTime.Now.Millisecond + "zzz")); // <ContractDate>

            // TODO: Add Null Checks.

            // <BasePayAmount>, <BasePayRate>
            // ID the term you're using so it can pull the right numbers - then pull based on IndexOf based on SelectedItem.
            // This will also help ID the correct APR to grab
            if (DealType == "Cash  ")
            {
                var taxAmount_convert = Convert.ToDouble(_mainWindow
                    .Get<TextBox>(SearchCriteria.ByAutomationId("txtSalesTax"))
                    .Text.Replace('%', ' '));

                var finalTax = taxAmount_convert / 100;

                var BasePayAmount = Convert.ToDouble(SellingPricetxt.Text) * finalTax +
                                    Convert.ToDouble(SellingPricetxt.Text);

                OriginalValues.Add(BasePayAmount.ToString());
                FinalPay = BasePayAmount.ToString();

                var BaseRate = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprBox")).Text;
                OriginalValues.Add(BaseRate);
            }
            else
            {
                var term = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("cbTerm"));
                termText = term.SelectedItemText;

                foreach (var term_items in term.Items) terms.Add(term_items.Text);

                var indexOfTerm = terms.IndexOf(term.SelectedItemText);
                switch (indexOfTerm)
                {
                    case 0:
                        var basePay0 = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("amountOne"));
                        var BasePayRate0 = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprOne"));
                        OriginalValues.Add(basePay0.Text);
                        OriginalValues.Add(BasePayRate0.Text);
                        FinalPay = basePay0.Text;
                        break;

                    case 1:
                        var basePay1 = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("amountTwo"));
                        var BasePayRate1 = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprTwo"));
                        OriginalValues.Add(basePay1.Text);
                        OriginalValues.Add(BasePayRate1.Text);
                        FinalPay = basePay1.Text;
                        break;
                    case 2:
                        var basePay2 = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("amountThree"));
                        var BasePayRate2 = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprThree"));
                        OriginalValues.Add(basePay2.Text);
                        OriginalValues.Add(BasePayRate2.Text);
                        FinalPay = basePay2.Text;
                        break;
                    case 3:
                        var basePay3 = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("amountFour"));
                        var BasePayRate3 = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprFour"));
                        OriginalValues.Add(basePay3.Text);
                        OriginalValues.Add(BasePayRate3.Text);
                        FinalPay = basePay3.Text;
                        break;
                }
            }

            // <CashDown>
            if (DealType == "Cash  ")
            {
                   var down = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDown"));
                OriginalValues.Add(down.Text);
            }
            else
            {
                var down = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDownpayment"));
                OriginalValues.Add(down.Text);
            }

            // <ClientType>
            OriginalValues.Add("Auto"); // Always Auto in the FI menu.

            // <DaysToPay>
            if (DealType == "Cash  ")
            {
                OriginalValues.Add("N/A");
            }
            else
            {
                var days = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDaysPay"));
                OriginalValues.Add(days.Text);
            }

            // <BaseType>
            OriginalValues.Add(DealType == "Cash  " ? "Cash" : "Loan");

            // <Name>
            OriginalValues.Add(DealType == "Cash  " ? "Cash" : "New");

            // <FIManager>
            var FI = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("txtManager"));
            OriginalValues.Add(string.IsNullOrEmpty(FI.SelectedItemText) ? " " : FI.SelectedItemText);

            // <FinalPay>
            OriginalValues.Add(FinalPay);

            // <LeaseAmountFinanced>
            OriginalValues.Add("0.00");

            // <MSRP>
            OriginalValues.Add("0");

            // <ResidualValue>
            OriginalValues.Add("0");

            // <SalesAssociate>
            var SA = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("txtSalesMan"));
            OriginalValues.Add(string.IsNullOrEmpty(SA.SelectedItemText) ? " " : SA.SelectedItemText);

            // <SellPrice>
            OriginalValues.Add(SellingPricetxt.Text);

            // <State>
            var xDocument = XDocument.Load(Path.GetDirectoryName(filepath) + @"\DealerShip.xml");
            var elements = from e_items in xDocument.Descendants("DealerShip") select e_items.Element("State");

            foreach (var itemElement in elements) OriginalValues.Add(itemElement.Value);

            // <Term>
            OriginalValues.Add(DealType == "Cash  "
                ? _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("termBox")).Text
                : termText);

            // <TotalAmount>
            if (DealType == "Cash  ")
            {
                OriginalValues.Add(FinalPay);
            }
            else
            {
                var sales_tax = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtSalesTax"))
                    .Text.Replace('%', ' ');

                var sales_tax_converted = Convert.ToDouble(sales_tax) / 100;
                var total = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblTotalFees"))
                    .Text.Replace('$', ' ');

                var sellingPrice = Convert.ToDouble(SellingPricetxt.Text);
                var tradeIn = Convert.ToDouble(TradeIntxt.Text);
                var downpay = Convert.ToDouble(downpaytxt.Text);

                var total_amount = Math.Abs((sellingPrice - tradeIn - downpay) * sales_tax_converted) +
                                   (sellingPrice - tradeIn - downpay) + Convert.ToDouble(total);

                OriginalValues.Add(total_amount.ToString());
            }

            // DealerParentID
            var xDocument_ID = XDocument.Load(Path.GetDirectoryName(filepath) + @"\NewSettings.xml");
            var elements_ID = from e_items in xDocument_ID.Descendants("Settings")
                where e_items.Element("SettingName").Value == "BiWeeklyDealerID"
                select e_items;

            foreach (var itemElement in elements_ID) OriginalValues.Add(itemElement.Element("SettingValue").Value);

            // <RatesProvider>
            OriginalValues.Add(comboBox1.Text); //TODO: Change this in the loop

            // Condition
            OriginalValues.Add(newRadio.Checked ? "New" : "Used");

            // <Mileage>
            OriginalValues.Add(odomttx.Text);

            // <Price>
            OriginalValues.Add(SellingPricetxt.Text);

            // <VIN>
            OriginalValues.Add(vintxt.Text);
        }

        public void RestartApplication()
        {
            // I made this little method so that when you call on something that changes a setting in the menu it will just restart the menu for you.

            // Process.Start -> Throwing Error:
            // (The Fix ^) In NewSettings.XML there is a field called OverrideWorkingFolder which needs to br set to True and SettingValue needs to be the Menu URL.
            var currentlocation = string.Empty;

            try
            {
                var processes = Process.GetProcessesByName("MainMenu");
                foreach (var process in processes) currentlocation = process.MainModule.FileName;

                _application.Kill();
                Process.Start(currentlocation);
            }
            catch (Exception ex)
            {
            }
        }



        // Validation For Using External OptionSoft DLL's-----
        internal static void CreateObjectGraph()
        {
            // DIContainer.Instance.Bind<IFilePathProvider>().To<FilePathProvider>().InSingletonScope();
            DIContainer.Instance.Bind<IExpandoSettings>().To<ExpandoSettings>().InSingletonScope();
            DIContainer.Instance.Bind<IFilePathProvider>().To<FilePathProvider>().InSingletonScope();
            DIContainer.Instance.Bind<IDealerInfo>().To<DealerInfo>().InSingletonScope();
            DIContainer.Instance.Bind<IProgramSetup>().To<ProgramSetup>().InSingletonScope();
            DIContainer.Instance.Bind<IApplication>().To<_Application>().InSingletonScope();

            // Must load license /before/ using DIContainer.Instance.Get<IApplication>().License but /after/ 
            // DIContainer.Instance.Bind<IFilePathProvider>()...
            DIContainer.Instance.Bind<IAprSetups>().To<AprSetups>().InSingletonScope();
            DIContainer.Instance.Bind<IPaymentDefault>().To<PaymentDefault>().InSingletonScope();
            DIContainer.Instance.Bind<ILoadSaveDeal>().To<LoadSaveDeal>().InSingletonScope();

            // DIContainer.Instance.Bind<IDealerInfo>().To<Data.DataProviders.DealerInfo>().InSingletonScope();
            DIContainer.Instance.Bind<IGapTypes>().To<GapTypes>().InSingletonScope();
            DIContainer.Instance.Bind<ILenderSetups>().To<LenderSetups>().InSingletonScope();

            // DIContainer.Instance.Bind<IProgramSetup>().To<ProgramSetup>().InSingletonScope();
            DIContainer.Instance.Bind<IManagers>().To<Managers>().InSingletonScope();
            DIContainer.Instance.Bind<IOtherSetups>().To<OtherSetups>().InSingletonScope();
            DIContainer.Instance.Bind<IPackageProducts>().To<PackageProducts>().InSingletonScope();
            DIContainer.Instance.Bind<IPrintOptions>().To<PrintOptions>().InSingletonScope();
            DIContainer.Instance.Bind<IPackages>().To<Packages>().InSingletonScope();
            DIContainer.Instance.Bind<ISetupPrivileges>().To<SetupButtonPrivileges>().InSingletonScope();
            DIContainer.Instance.Bind<ITaxRebate>().To<TaxRebate>().InSingletonScope();
            DIContainer.Instance.Bind<IVSCOptions>().To<VSCOptions>().InSingletonScope();
            DIContainer.Instance.Bind<InsuranceInterface>().To<Insurances>().InSingletonScope();

            UserData.LoadEffectiveParentID();

            if (DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).IsDMSEnabled &&
                DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).FeesFromService)
                DIContainer.Instance.Bind<IFeeSetups>()
                    .To<DmsFeeSetups>()
                    .InSingletonScope()
                    .WithConstructorArgument(UserData.EffectiveParentID);
            else
                DIContainer.Instance.Bind<IFeeSetups>().To<FeeSetups>().InSingletonScope();

            if (DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).IsDMSEnabled &&
                DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).RebatesFromService)
                DIContainer.Instance.Bind<IRebateSetups>()
                    .To<DmsRebateSetups>()
                    .InSingletonScope()
                    .WithConstructorArgument(UserData.EffectiveParentID);
            else
                DIContainer.Instance.Bind<IRebateSetups>().To<RebateSetups>().InSingletonScope();

            LeaseRateSetups.Instance.Load();

            var datFilePath = DIContainer.Instance.Get<IFilePathProvider>().AddToWorkingPath("dealership.dat");

            try
            {
                DIContainer.Instance.Get<IApplication>().License = new License(datFilePath);
                DIContainer.Instance.Get<IApplication>().License.OpenLicense();
            }
            catch (LicenseFileNotFoundException)
            {
            }
        }

        public string ReadPdfFile(string fileName)
        {
            var text = new StringBuilder();

            if (File.Exists(fileName))
            {
                var pdfReader = new PdfReader(fileName);

                for (var page = 1; page <= pdfReader.NumberOfPages; page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    var currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);

                    currentText = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8,
                        Encoding.Default.GetBytes(currentText)));

                    text.Append(currentText);
                }

                pdfReader.Close();
            }

            return text.ToString();
        }

        /// <summary>
        ///     Creates a DateTime format that is expected by MYSQL so that the Database table will be happy when it is input.
        /// </summary>
        /// <returns></returns>
        private static string CurrentDateTimeIso()
        {
            var formatForMySql = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            return formatForMySql;
        }

        /// <summary>
        ///     Used for formatting string correctly for the database table.
        /// </summary>
        /// <param name="str">String to escape</param>
        /// <returns></returns>
        private static string MySqlEscape(string str)
        {
            return Regex.Replace(str, @"[\x00'""\b\n\r\t\cZ\\%_]", delegate(Match match)
            {
                var v = match.Value;
                switch (v)
                {
                    case "\x00": // ASCII NUL (0x00) character
                        return "\\0";
                    case "\b": // BACKSPACE character
                        return "\\b";
                    case "\n": // NEWLINE (linefeed) character
                        return "\\n";
                    case "\r": // CARRIAGE RETURN character
                        return "\\r";
                    case "\t": // TAB
                        return "\\t";
                    case "\u001A": // Ctrl-Z
                        return "\\Z";
                    default:
                        return "\\" + v;
                }
            });
        }

        private static string ProcessXml(string document, string[] xml_data)
        {
            var doc = XDocument.Load(document);

            foreach (var data_name in xml_data)
            {
                var grab_data = doc.Descendants(data_name);

                foreach (var values in grab_data)
                    if (string.IsNullOrEmpty(values.Value))
                        document += "null" + ",";
                    else
                        document += values.Value + ",";
            }

            return document;
        }

        private static XElement removeAllNamespaces(XElement xmlDocument)
        {
            var stripped = new XElement(xmlDocument.Name.LocalName);
            foreach (var attribute in xmlDocument.Attributes()
                .Where(attribute =>
                    !attribute.IsNamespaceDeclaration && string.IsNullOrEmpty(attribute.Name.NamespaceName)))
                stripped.Add(new XAttribute(attribute.Name.LocalName, attribute.Value));

            if (!xmlDocument.HasElements)
            {
                stripped.Value = xmlDocument.Value;
                return stripped;
            }

            stripped.Add(xmlDocument.Elements().Select(el => StripNS(el)));
            return stripped;
        }

        /// <summary>
        ///     Replaces the brackets in HTML table so that they can be properly displayed on web page.
        /// </summary>
        /// <param name="s">string that contains the HTML table.</param>
        /// <returns></returns>
        private static string ReplaceBrackets(string s)
        {
            var final = s.Replace("<", "&lt;").Replace(">", "&gt;");
            return final;
        }

        /// <summary>
        ///     Strips away the unused namespaces from XML file.
        /// </summary>
        /// <param name="xmlDocument">The XML document.</param>
        /// <returns></returns>
        private static XElement StripNS(XElement xmlDocument)
        {
            var xmlDocumentWithoutNs = removeAllNamespaces(xmlDocument);
            return xmlDocumentWithoutNs;
        }

        /// <summary>
        ///     Uploads all needed values into database for ther reports site.
        /// </summary>
        /// <returns></returns>
        private static string UploadtoDb()
        {
            for (var i = 0; i < reportList.Count; i++)
            {
                var query = "INSERT INTO `automation-reports` (`id`, `reportid`, `report`, `success`, `date`)" +
                            "VALUES (NULL, " + Settings.Default.currentid + ", '" + reportList[i].Item1 + "' , '" +
                            reportList[i].Item2 + "' , '" + CurrentDateTimeIso() + "')";

                var databaseConnection = new MySqlConnection(connectionString);
                var commandDatabase = new MySqlCommand(query, databaseConnection) {CommandTimeout = 60};

                try
                {
                    databaseConnection.Open();
                    var myReader = commandDatabase.ExecuteReader();
                    databaseConnection.Close();
                    Settings.Default.currentid += 1; // Update ID postion.
                    Settings.Default.Save();
                    return "GOOD";
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }

            return null;
        }

        private void Contract()
        {
            CheckHook(); // Check if menu is hooked.

            foreach (ListViewItem item in listView1.Items)
            {
                FillOut(item.SubItems[1].Text, Convert.ToDouble(item.SubItems[2].Text), Convert.ToDouble(item.SubItems[3].Text),
                    Convert.ToDouble(item.SubItems[4].Text), Convert.ToDouble(item.SubItems[5].Text),
                    Convert.ToDouble(item.SubItems[6].Text), Convert.ToInt32(item.SubItems[7].Text));

                #region GetProducts

                SetupAutomation();

                var nameMapping = new Dictionary<string, int>
                {
                    {"prodOne", 1},
                    {"prodTwo", 2},
                    {"prodThree", 3},
                    {"prodFour", 4},
                    {"prodFive", 5},
                    {"prodSix", 6},
                    {"prodSeven", 7},
                    {"prodEight", 8}
                };

                var checkMapping = new Dictionary<string, int>
                {
                    {"prodBox1", 1},
                    {"prodBox2", 2},
                    {"prodBox3", 3},
                    {"prodBox4", 4},
                    {"prodBox5", 5},
                    {"prodBox6", 6},
                    {"prodBox7", 7},
                    {"prodBox8", 8}
                };

                var priceMapping = new Dictionary<string, int>
                {
                    {"txtProdBox1", 1},
                    {"txtProdBox2", 2},
                    {"txtProdBox3", 3},
                    {"txtProdBox4", 4},
                    {"txtProdBox5", 5},
                    {"txtProdBox6", 6},
                    {"txtProdBox7", 7},
                    {"txtProdBox8", 8}
                };

                // Get Payment info ->
                // Not sure why but the first element is last. So like 2,3,4,5,1
                var paymentPanel = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("panel2"));
                var paymentCheckBoxes = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.CheckBox))
                    .Cast<CheckBox>()
                    .ToList();

                var planNames = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
                var prices = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.Edit))
                    .Cast<TextBox>()
                    .ToList();

                var orderedNames = planNames.OrderBy(x => nameMapping[x.Id]).ToList();
                var orderedCheckboxs = paymentCheckBoxes.OrderBy(x => checkMapping[x.Id]).ToList();
                var orderedPrices = prices.OrderBy(x => priceMapping[x.Id]).ToList();

                for (var i = 0; i < orderedNames.Count; i++)
                {
                    var rating = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("btnRating" + (i + 1)));
                    rating.Click();
                    SetupAutomation();

                    try
                    {
                        var popup = _mainWindow.Popup; // Grab the context menu
                        var items = popup.ItemBy(
                            SearchCriteria.ByText(item.Text)); // Find whatever you're looking for.

                        items.Click(); // Select.
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (_mainWindow.Exists<Button>(SearchCriteria.ByAutomationId("btnCancel")))
                        {
                            var cancel = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnCancel"));
                            cancel.Click();
                        }
                        else
                        {
                            SendKeys.SendWait("{ENTER}");
                        }

                        SetupAutomation();
                        Debug.WriteLine("Not found in btnRating" + (i + 1));

                        if (i + 1 == orderedNames.Count) MessageBox.Show("Could not find specified item.");
                    }
                }

                SetupAutomation();

                #endregion

                // | |
                // | |
                // V V

                #region Pending...

                //switch (comboBox1.Text)
                //{
                //    case "Premier Mechanical Breakdown Protection":
                //    {
                //            {
                //                Start();

                //                // Error(Reverse ?) Catching
                //                if (GrabErrorOrExucute())
                //                {
                //                    // Throw Error Within GrabErrorOrExecute
                //                }

                //                var VIN = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtVIN"));
                //                VIN.Text = vintxt.Text;

                //                // Set Odometer
                //                var odom = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlOdometer"));
                //                var two = odom.Get(SearchCriteria.ByAutomationId("nudOdometer"));
                //                two.Click();

                //                SendKeys.SendWait(odomttx.Text); // Well I have no idea how else to do this. I cannot select the Odometer box to set the text via TestStack.

                //                SendKeys.SendWait("{Enter}");

                //                if (_mainWindow.Exists<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice")))
                //                {
                //                    var salePane = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice"));
                //                    var salePrice = salePane.Get(SearchCriteria.ByAutomationId("nudVehicleSalePrice"));
                //                    salePrice.Click();
                //                    SendKeys.SendWait(SellingPricetxt.Text);
                //                    SendKeys.SendWait("{TAB}");
                //                }

                //                // Get Rates
                //                var okbutton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                //                okbutton.Click();

                //                // Error(Reverse ?) Catching
                //                if (GrabErrorOrExucute())
                //                {
                //                    // Throw Error Within GrabErrorOrExecute
                //                }
                //                else
                //                {
                //                    OriginalValues.Clear(); // Make sure we have a clean list.
                //                    GrabRate();

                //                    // Fill Data
                //                    FillTestDate();

                //                    // ProcessXML
                //                    WriteXMLDataToFile("request.xml", ResponseType.requestOnly);

                //                    //Populate XML Compare
                //                    PopulateCompareView();

                //                    // Color Code them and compare ->
                //                    for (var i = 0; i < form2.listView1.Items.Count; i++)
                //                    {
                //                        // Search it in the second listview
                //                        var item_1 = form2.listView1.Items[i].Text;
                //                        var item_2 = form2.listView2.Items[i].Text;

                //                        // If not found...
                //                        if (item_2 == item_1)
                //                        {
                //                            form2.listView2.Items[i].BackColor = Color.LightGreen;
                //                            form2.listView1.Items[i].BackColor = Color.LightGreen;
                //                        }
                //                        else
                //                        {
                //                            form2.listView2.Items[i].BackColor = Color.IndianRed;
                //                            form2.listView1.Items[i].BackColor = Color.IndianRed;
                //                        }
                //                    }

                //                    // Write our ListView to a HTML table.
                //                    if (form2.useHTMLtable.Checked)
                //                    {
                //                        var table = ListViewToHtmlTable(form2.listView1, 1, 1, 2);
                //                        var table2 = ListViewToHtmlTable(form2.listView2, 1, 1, 2);
                //                        File.WriteAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html",
                //                            table + " " + table2);
                //                    }

                //                    // DB processing ->
                //                    var report = File.ReadAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html");

                //                    // If there's an error
                //                    for (var i = 0; i < form2.listView1.Items.Count; i++)
                //                    {
                //                        var compare = form2.listView1.Items[i].Text.ToLower();
                //                        var compareTwo = form2.listView2.Items[i].Text.ToLower();
                //                        if (compare != compareTwo)
                //                            reportList.Add(new Tuple<string, int>(MySqlEscape(report), 1));
                //                    }

                //                    // If not
                //                    reportList.Add(new Tuple<string, int>(MySqlEscape(report), 0));

                //                    UploadtoDb();
                //                    CloseandReset(0);
                //                }

                //                break;

                //            }
                //        }

                //    case "Premier Classic":
                //        {
                //            {
                //                Start();

                //                // Error(Reverse ?) Catching
                //                if (GrabErrorOrExucute())
                //                {
                //                    // Throw Error Within GrabErrorOrExecute
                //                }

                //                var VIN = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtVIN"));
                //                VIN.Text = vintxt.Text;

                //                // Set Odometer
                //                var odom = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlOdometer"));
                //                var two = odom.Get(SearchCriteria.ByAutomationId("nudOdometer"));
                //                two.Click();

                //                SendKeys.SendWait(odomttx.Text); // Well I have no idea how else to do this. I cannot select the Odometer box to set the text via TestStack.

                //                SendKeys.SendWait("{Enter}");

                //                if (_mainWindow.Exists<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice")))
                //                {
                //                    var salePane = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice"));
                //                    var salePrice = salePane.Get(SearchCriteria.ByAutomationId("nudVehicleSalePrice"));
                //                    salePrice.Click();
                //                    SendKeys.SendWait(SellingPricetxt.Text);
                //                    SendKeys.SendWait("{TAB}");
                //                }

                //                // Get Rates
                //                var okbutton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                //                okbutton.Click();

                //                // Error(Reverse ?) Catching
                //                if (GrabErrorOrExucute())
                //                {
                //                    // Throw Error Within GrabErrorOrExecute
                //                }
                //                else
                //                {
                //                    OriginalValues.Clear(); // Make sure we have a clean list.
                //                    GrabRate();

                //                    // Fill Data
                //                    FillTestDate();

                //                    // ProcessXML
                //                    WriteXMLDataToFile("request.xml", ResponseType.requestOnly);

                //                    //Populate XML Compare
                //                    PopulateCompareView();

                //                    // Color Code them and compare ->
                //                    for (var i = 0; i < form2.listView1.Items.Count; i++)
                //                    {
                //                        // Search it in the second listview
                //                        var item_1 = form2.listView1.Items[i].Text;
                //                        var item_2 = form2.listView2.Items[i].Text;

                //                        // If not found...
                //                        if (item_2 == item_1)
                //                        {
                //                            form2.listView2.Items[i].BackColor = Color.LightGreen;
                //                            form2.listView1.Items[i].BackColor = Color.LightGreen;
                //                        }
                //                        else
                //                        {
                //                            form2.listView2.Items[i].BackColor = Color.IndianRed;
                //                            form2.listView1.Items[i].BackColor = Color.IndianRed;
                //                        }
                //                    }

                //                    // Write our ListView to a HTML table.
                //                    if (form2.useHTMLtable.Checked)
                //                    {
                //                        var table = ListViewToHtmlTable(form2.listView1, 1, 1, 2);
                //                        var table2 = ListViewToHtmlTable(form2.listView2, 1, 1, 2);
                //                        File.WriteAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html",
                //                            table + " " + table2);
                //                    }

                //                    // DB processing ->
                //                    var report = File.ReadAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html");

                //                    // If there's an error
                //                    for (var i = 0; i < form2.listView1.Items.Count; i++)
                //                    {
                //                        var compare = form2.listView1.Items[i].Text.ToLower();
                //                        var compareTwo = form2.listView2.Items[i].Text.ToLower();
                //                        if (compare != compareTwo)
                //                            reportList.Add(new Tuple<string, int>(MySqlEscape(report), 1));
                //                    }

                //                    // If not
                //                    reportList.Add(new Tuple<string, int>(MySqlEscape(report), 0));

                //                    UploadtoDb();
                //                    CloseandReset(0);
                //                }

                //                break;

                //            }
                //        }

                //    case "Warrantech Mechanical Breakdown Protection":
                //    {
                //        Start();

                //        // Error(Reverse ?) Catching
                //        if (GrabErrorOrExucute())
                //        {
                //            // Throw Error Within GrabErrorOrExecute
                //        }

                //        var VIN = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtVIN"));
                //        VIN.Text = vintxt.Text;

                //        // Set Odometer
                //        var odom = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlOdometer"));
                //        var two = odom.Get(SearchCriteria.ByAutomationId("nudOdometer"));
                //        two.Click();

                //        SendKeys.SendWait(odomttx
                //            .Text); // Well I have no idea how else to do this. I cannot select the Odometer box to set the text via TestStack.

                //        SendKeys.SendWait("{Enter}");

                //        // Get Rates
                //        var okbutton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                //        okbutton.Click();

                //        // Error(Reverse ?) Catching
                //        if (GrabErrorOrExucute())
                //        {
                //            // Throw Error Within GrabErrorOrExecute
                //        }
                //        else
                //        {
                //            OriginalValues.Clear(); // Make sure we have a clean list.
                //            GrabRate();

                //            // Fill Data
                //            FillTestDate();

                //            // ProcessXML
                //            WriteXMLDataToFile("request.xml", ResponseType.requestOnly);

                //            //Populate XML Compare
                //            PopulateCompareView();

                //            // Color Code them and compare ->
                //            for (var i = 0; i < form2.listView1.Items.Count; i++)
                //            {
                //                // Search it in the second listview
                //                var item_1 = form2.listView1.Items[i].Text;
                //                var item_2 = form2.listView2.Items[i].Text;

                //                // If not found...
                //                if (item_2 == item_1)
                //                {
                //                    form2.listView2.Items[i].BackColor = Color.LightGreen;
                //                    form2.listView1.Items[i].BackColor = Color.LightGreen;
                //                }
                //                else
                //                {
                //                    form2.listView2.Items[i].BackColor = Color.IndianRed;
                //                    form2.listView1.Items[i].BackColor = Color.IndianRed;
                //                }
                //            }

                //            // Write our ListView to a HTML table.
                //            if (form2.useHTMLtable.Checked)
                //            {
                //                var table = ListViewToHtmlTable(form2.listView1, 1, 1, 2);
                //                var table2 = ListViewToHtmlTable(form2.listView2, 1, 1, 2);
                //                File.WriteAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html",
                //                    table + " " + table2);
                //            }

                //            // DB processing ->
                //            var report = File.ReadAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html");

                //            // If there's an error
                //            for (var i = 0; i < form2.listView1.Items.Count; i++)
                //            {
                //                var compare = form2.listView1.Items[i].Text.ToLower();
                //                var compareTwo = form2.listView2.Items[i].Text.ToLower();
                //                if (compare != compareTwo)
                //                    reportList.Add(new Tuple<string, int>(MySqlEscape(report), 1));
                //            }

                //            // If not
                //            reportList.Add(new Tuple<string, int>(MySqlEscape(report), 0));

                //            UploadtoDb();
                //            CloseandReset(0);
                //        }

                //        break;
                //    }

                //    case "Safeguard Interior Exterior Protection":
                //    {
                //        Start();

                //        // Error(Reverse ?) Catching
                //        if (GrabErrorOrExucute())
                //        {
                //            // Throw Error Within GrabErrorOrExecute
                //        }

                //        var VIN = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtVIN"));
                //        VIN.Text = vintxt.Text;

                //        // Set Odometer
                //        var odom = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlOdometer"));
                //        var two = odom.Get(SearchCriteria.ByAutomationId("nudOdometer"));
                //        two.Click();

                //        SendKeys.SendWait(odomttx.Text); // Well I have no idea how else to do this. I cannot select the Odometer box to set the text via TestStack.

                //        SendKeys.SendWait("{Enter}");

                //        if (_mainWindow.Exists<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice")))
                //        {
                //            var salePane = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice"));
                //            var salePrice = salePane.Get(SearchCriteria.ByAutomationId("nudVehicleSalePrice"));
                //            salePrice.Click();
                //            SendKeys.SendWait(SellingPricetxt.Text);
                //            SendKeys.SendWait("{TAB}");
                //        }

                //            // Get Rates
                //        var okbutton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                //        okbutton.Click();

                //        // Error(Reverse ?) Catching
                //        if (GrabErrorOrExucute())
                //        {
                //            // Throw Error Within GrabErrorOrExecute
                //        }
                //        else
                //        {
                //            OriginalValues.Clear(); // Make sure we have a clean list.
                //            GrabRate();

                //            // Fill Data
                //            FillTestDate();

                //            // ProcessXML
                //            WriteXMLDataToFile("request.xml", ResponseType.requestOnly);

                //            //Populate XML Compare
                //            PopulateCompareView();

                //            // Color Code them and compare ->
                //            for (var i = 0; i < form2.listView1.Items.Count; i++)
                //            {
                //                // Search it in the second listview
                //                var item_1 = form2.listView1.Items[i].Text;
                //                var item_2 = form2.listView2.Items[i].Text;

                //                // If not found...
                //                if (item_2 == item_1)
                //                {
                //                    form2.listView2.Items[i].BackColor = Color.LightGreen;
                //                    form2.listView1.Items[i].BackColor = Color.LightGreen;
                //                }
                //                else
                //                {
                //                    form2.listView2.Items[i].BackColor = Color.IndianRed;
                //                    form2.listView1.Items[i].BackColor = Color.IndianRed;
                //                }
                //            }

                //            // Write our ListView to a HTML table.
                //            if (form2.useHTMLtable.Checked)
                //            {
                //                var table = ListViewToHtmlTable(form2.listView1, 1, 1, 2);
                //                var table2 = ListViewToHtmlTable(form2.listView2, 1, 1, 2);
                //                File.WriteAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html",
                //                    table + " " + table2);
                //            }

                //            // DB processing ->
                //            var report = File.ReadAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html");

                //            // If there's an error
                //            for (var i = 0; i < form2.listView1.Items.Count; i++)
                //            {
                //                var compare = form2.listView1.Items[i].Text.ToLower();
                //                var compareTwo = form2.listView2.Items[i].Text.ToLower();
                //                if (compare != compareTwo)
                //                    reportList.Add(new Tuple<string, int>(MySqlEscape(report), 1));
                //            }

                //            // If not
                //            reportList.Add(new Tuple<string, int>(MySqlEscape(report), 0));

                //            UploadtoDb();
                //            CloseandReset(0);
                //        }

                //        break;

                //    }

                //    case "CareGard Mechanical Breakdown Protection":
                //        {
                //            Start();

                //            // Error(Reverse ?) Catching
                //            if (GrabErrorOrExucute())
                //            {
                //                // Throw Error Within GrabErrorOrExecute
                //            }

                //            var VIN = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtVIN"));
                //            VIN.Text = vintxt.Text;

                //            // Set Odometer
                //            var odom = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlOdometer"));
                //            var two = odom.Get(SearchCriteria.ByAutomationId("nudOdometer"));
                //            two.Click();

                //            SendKeys.SendWait(odomttx.Text); // Well I have no idea how else to do this. I cannot select the Odometer box to set the text via TestStack.

                //            SendKeys.SendWait("{Enter}");

                //            if (_mainWindow.Exists<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice")))
                //            {
                //                var salePane = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice"));
                //                var salePrice = salePane.Get(SearchCriteria.ByAutomationId("nudVehicleSalePrice"));
                //                salePrice.Click();
                //                SendKeys.SendWait(SellingPricetxt.Text);
                //                SendKeys.SendWait("{TAB}");
                //            }

                //            // Get Rates
                //            var okbutton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                //            okbutton.Click();

                //            // Error(Reverse ?) Catching
                //            if (GrabErrorOrExucute())
                //            {
                //                // Throw Error Within GrabErrorOrExecute
                //            }
                //            else
                //            {
                //                OriginalValues.Clear(); // Make sure we have a clean list.
                //                GrabRate();

                //                // Fill Data
                //                FillTestDate();

                //                // ProcessXML
                //                WriteXMLDataToFile("request.xml", ResponseType.requestOnly);

                //                //Populate XML Compare
                //                PopulateCompareView();

                //                // Color Code them and compare ->
                //                for (var i = 0; i < form2.listView1.Items.Count; i++)
                //                {
                //                    // Search it in the second listview
                //                    var item_1 = form2.listView1.Items[i].Text;
                //                    var item_2 = form2.listView2.Items[i].Text;

                //                    // If not found...
                //                    if (item_2 == item_1)
                //                    {
                //                        form2.listView2.Items[i].BackColor = Color.LightGreen;
                //                        form2.listView1.Items[i].BackColor = Color.LightGreen;
                //                    }
                //                    else
                //                    {
                //                        form2.listView2.Items[i].BackColor = Color.IndianRed;
                //                        form2.listView1.Items[i].BackColor = Color.IndianRed;
                //                    }
                //                }

                //                // Write our ListView to a HTML table.
                //                if (form2.useHTMLtable.Checked)
                //                {
                //                    var table = ListViewToHtmlTable(form2.listView1, 1, 1, 2);
                //                    var table2 = ListViewToHtmlTable(form2.listView2, 1, 1, 2);
                //                    File.WriteAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html",
                //                        table + " " + table2);
                //                }

                //                // DB processing ->
                //                var report = File.ReadAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html");

                //                // If there's an error
                //                for (var i = 0; i < form2.listView1.Items.Count; i++)
                //                {
                //                    var compare = form2.listView1.Items[i].Text.ToLower();
                //                    var compareTwo = form2.listView2.Items[i].Text.ToLower();
                //                    if (compare != compareTwo)
                //                        reportList.Add(new Tuple<string, int>(MySqlEscape(report), 1));
                //                }

                //                // If not
                //                reportList.Add(new Tuple<string, int>(MySqlEscape(report), 0));

                //                UploadtoDb();
                //                CloseandReset(0);
                //            }

                //            break;

                //        }
                //}

                #endregion

                {
                    {
                        Start();

                        // Error(Reverse ?) Catching
                        if (GrabErrorOrExucute())
                        {
                            // Throw Error Within GrabErrorOrExecute
                        }

                        var VIN = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtVIN"));
                        VIN.Text = item.SubItems[8].Text;

                        // Set Odometer
                        var odom = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlOdometer"));
                        var two = odom.Get(SearchCriteria.ByAutomationId("nudOdometer"));
                        two.Click();

                        SendKeys.SendWait(item.SubItems[9].Text); // Well I have no idea how else to do this. I cannot select the Odometer box to set the text via TestStack.

                        SendKeys.SendWait("{Enter}");

                        if (_mainWindow.Exists<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice")))
                        {
                            var salePane = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("pnlVehicleSalePrice"));
                            var salePrice = salePane.Get(SearchCriteria.ByAutomationId("nudVehicleSalePrice"));
                            salePrice.Click();
                            //SendKeys.SendWait(SellingPricetxt.Text);
                            SendKeys.SendWait("{TAB}");
                        }

                        // Get Rates
                        var okbutton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                        okbutton.Click();

                        // Error(Reverse ?) Catching
                        if (GrabErrorOrExucute())
                        {
                            // Throw Error Within GrabErrorOrExecute
                        }
                        else
                        {
                            OriginalValues.Clear(); // Make sure we have a clean list.
                            GrabRate();


                            var MenuMB = _mainWindow.MenuBar;
                            var openPDF = MenuMB.MenuItem("Contracts", item.Text, "Open PDF");
                            openPDF.Click();

                            // Fill Data
                            FillTestDate();

                            // ProcessXML
                            WriteXMLDataToFile("request.xml", ResponseType.requestOnly);

                            
                            //Populate XML Compare
                            PopulateCompareView();

                            // Color Code them and compare ->
                            for (var i = 0; i < form2.listView1.Items.Count; i++)
                            {
                                // Search it in the second listview
                                var item_1 = form2.listView1.Items[i].Text;
                                var item_2 = form2.listView2.Items[i].Text;

                                // If not found...
                                if (item_2 == item_1)
                                {
                                    form2.listView2.Items[i].BackColor = Color.LightGreen;
                                    form2.listView1.Items[i].BackColor = Color.LightGreen;
                                }
                                else
                                {
                                    form2.listView2.Items[i].BackColor = Color.IndianRed;
                                    form2.listView1.Items[i].BackColor = Color.IndianRed;
                                }
                            }

                            // Write our ListView to a HTML table.
                            if (form2.useHTMLtable.Checked)
                            {
                                var table = ListViewToHtmlTable(form2.listView1, 1, 1, 2);
                                var table2 = ListViewToHtmlTable(form2.listView2, 1, 1, 2);
                                File.WriteAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html",
                                    table + " " + table2);
                            }

                            // DB processing ->
                            var report = File.ReadAllText(Environment.CurrentDirectory + @"\RatingSnapshot.html");

                            // If there's an error
                            for (var i = 0; i < form2.listView1.Items.Count; i++)
                            {
                                var compare = form2.listView1.Items[i].Text.ToLower();
                                var compareTwo = form2.listView2.Items[i].Text.ToLower();
                                if (compare != compareTwo)
                                    reportList.Add(new Tuple<string, int>(MySqlEscape(report), 1));
                            }

                            // If not
                            reportList.Add(new Tuple<string, int>(MySqlEscape(report), 0));

                            UploadtoDb();

                            CloseandReset(1);

                        }
                    }
                }


            }
        }
        public void WriteXMLDataToFile(string filename, ResponseType type)
        {
            switch (type)
            {
                case ResponseType.requestOnly:
                {
                    File.WriteAllText(Environment.CurrentDirectory + @"\" + filename, XMLRequest,
                        Encoding.UTF8); // Write Raw Request to file

                    File.WriteAllLines(Environment.CurrentDirectory + @"\" + filename,
                        File.ReadAllLines(Environment.CurrentDirectory + @"\" + filename).Skip(9));

                    DoRemovespace(Environment.CurrentDirectory + @"\" + filename);
                    var data = StripNS(XElement.Parse(File.ReadAllText(Environment.CurrentDirectory + @"\" + filename)))
                        .ToString();

                    File.WriteAllText(Environment.CurrentDirectory + @"\" + filename, data,
                        Encoding.UTF8); // Write Raw Request to file, stripped of namespaces.

                    break;
                }
                case ResponseType.responseOnly:
                {
                    File.WriteAllText(Environment.CurrentDirectory + @"\" + filename,
                        "<?xml version='1.0' encoding='utf - 8'?>" + Environment.NewLine + responseString,
                        Encoding.UTF8); // Write Raw Request to file

                    DoRemovespace(Environment.CurrentDirectory + @"\" + filename);
                    var data = StripNS(XElement.Parse(File.ReadAllText(Environment.CurrentDirectory + @"\" + filename)))
                        .ToString();

                    File.WriteAllText(Environment.CurrentDirectory + @"\" + filename, data,
                        Encoding.UTF8); // Write Raw Request to file, stripped of namespaces.

                    break;
                }
                case ResponseType.both:
                {
                    WriteXMLDataToFile("request.xml", ResponseType.requestOnly);
                    WriteXMLDataToFile("response.xml", ResponseType.responseOnly);
                    break;
                }
            }
        }

        /// <summary>
        ///     Calculates the final payment from OptionSoft DLL.
        /// </summary>
        /// <param name="saleAmount">The intial sale amount.</param>
        /// <param name="fees">Total amount of fees.</param>
        /// <param name="termLength">Length of the term. (66, 72, etc).</param>
        /// <param name="termApr">The APR on term.</param>
        /// <param name="trade">The total trade amount.</param>
        /// <param name="payoff">The total payoff amount.</param>
        /// <param name="down">The total downpayment.</param>
        /// <param name="productTotal">The total of products selected.</param>
        /// <param name="productTax">The total of taxes on products. Uses sales tax as default.</param>
        /// <param name="taxOverride">The Tax override if you want it to be something else other than the sales tax amount.</param>
        /// <param name="type">The type: "final" or "tot" (Which is payments without products) </param>
        /// <returns></returns>
        public static double CalcFinalPaymentFromOTI(double saleAmount, double fees, double termLength, double termApr,
            double trade, double payoff, double down, double productTotal, double productTax, double taxOverride,
            string type)
        {
            var theDeal = new MenuDeal();

            var basePayment = theDeal.CalcFinalPaymentNew(saleAmount + fees, termLength, termApr, trade, payoff, down,
                productTotal, productTax, taxOverride, type);

            return basePayment;
        }

        /// <summary>
        ///     Checks to see if application is currently hooked into the Main Menu.
        /// </summary>
        private void CheckHook()
        {
            if (_mainWindow.Enabled) return;
            SetupAutomation();
        }

        /// <summary>
        ///     Close and restart menu.
        /// </summary>
        /// <param name="forceReset">1 = yes, 0 = No</param>
        private void CloseandReset(int forceReset)
        {
            if (forceReset == 0)
            {
                // forceReset is mant for regression testing, 0 = no, 1 = yes.
                var result = MessageBox.Show("Would you like to close and reset this deal now?", "Reset Deal",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                switch (result)
                {
                    case DialogResult.Yes:
                        var MenuMB = _mainWindow.MenuBar;
                        var resetItem = MenuMB.MenuItem("File", "Reset Deal");
                        resetItem.Click();
                        SetupAutomation();
                        break;
                }
            }
            else
            {

                _mainWindow.Click();
                var MenuMB = _mainWindow.MenuBar;
                var resetItem = MenuMB.MenuItem("File","Reset Deal");
                resetItem.Click();
                SetupAutomation();

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void Fiddler_AfterSession(Session s)
        {
            // Ignore HTTPS connect requests
            if (s.RequestMethod == "CONNECT") return;

            if (CaptureConfiguration.ProcessId > 0)
                if (s.LocalProcessID != 0 && s.LocalProcessID != CaptureConfiguration.ProcessId)
                    return;

            if (!string.IsNullOrEmpty(CaptureConfiguration.CaptureDomain))
                if (s.hostname.ToLower() != CaptureConfiguration.CaptureDomain.Trim().ToLower())
                    return;

            if (CaptureConfiguration.IgnoreResources)
            {
                var url = s.fullUrl.ToLower();

                var extensions = CaptureConfiguration.ExtensionFilterExclusions;
                foreach (var ext in extensions)
                    if (url.Contains(ext))
                        return;

                var filters = CaptureConfiguration.UrlFilterExclusions;
                foreach (var urlFilter in filters)
                    if (url.Contains(urlFilter))
                        return;
            }

            if (s == null || s.oRequest == null || s.oRequest.headers == null) return;

            var headers = s.oRequest.headers.ToString();
            var reqBody = Encoding.UTF8.GetString(s.RequestBody);

            // if you wanted to capture the response
            var respHeaders = s.oResponse.headers.ToString();
            var respBody = Encoding.UTF8.GetString(s.ResponseBody);

            // replace the HTTP line to inject full URL
            var firstLine = s.RequestMethod + " " + s.fullUrl + " " + s.oRequest.headers.HTTPVersion;
            var at = headers.IndexOf("\r\n");
            if (at < 0) return;

            headers = firstLine + "\r\n" + headers.Substring(at + 1);

            var output = headers + "\r\n" + (!string.IsNullOrEmpty(reqBody) ? reqBody + "\r\n" : string.Empty) +
                         Separator + "\r\n\r\n";

            // So the UI doesn't lag.
            BeginInvoke(new Action<string>(text =>
            {
                XMLRequest = text;
                Debug.WriteLine(text);
            }), output);

            responseString = respBody;
        }

        /// <summary>
        ///     Fills out the Main Menu fields on the main dealscreen.
        /// </summary>
        /// <param name="clientName">Name of the client.</param>
        /// <param name="sellingPrice">The selling price.</param>
        /// <param name="tradePrice">The trade price.</param>
        /// <param name="payoffPrice">The payoff price.</param>
        /// <param name="downpaymentPrice">The downpayment price.</param>
        /// <param name="rebatePrice">The rebate price.</param>
        /// <param name="daystoPay">The days to pay.</param>
        private void FillOut(string clientName, double sellingPrice, double tradePrice, double payoffPrice,
            double downpaymentPrice, double rebatePrice, int daystoPay)
        {
            // Client Name
            if (string.IsNullOrWhiteSpace(ClientNametxt.Text))
            {
            }
            else
            {
                var client = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtName"));
                client.Text = clientName;
            }

            // Selling Price
            if (SellingPricetxt.Text == "0" || string.IsNullOrEmpty(SellingPricetxt.Text))
            {
            }
            else
            {
                var price = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtPrice"));
                price.Text = sellingPrice.ToString();
                price.KeyIn(KeyboardInput.SpecialKeys.RETURN); // Format $
            }

            if (TradeIntxt.Text == "0" || string.IsNullOrEmpty(TradeIntxt.Text))
            {
            }
            else
            {
                // Trade Price
                var trade = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtTrade"));
                trade.Text = tradePrice.ToString();
                trade.KeyIn(KeyboardInput.SpecialKeys.RETURN);
            }

            if (Payofftxt.Text == "0" || string.IsNullOrEmpty(Payofftxt.Text))
            {
            }
            else
            {
                // Payoff Price
                var payoff = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtPayoff"));
                payoff.Text = payoffPrice.ToString();
                payoff.KeyIn(KeyboardInput.SpecialKeys.RETURN);
            }

            if (downpaytxt.Text == "0" || string.IsNullOrEmpty(downpaytxt.Text))
            {
            }
            else
            {
                // Downpayment Price
                var down = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDown"));
                down.Text = downpaymentPrice.ToString();
                down.KeyIn(KeyboardInput.SpecialKeys.RETURN);
            }

            if (Rebatetxt.Text == "0" || string.IsNullOrEmpty(Rebatetxt.Text))
            {
            }
            else
            {
                // Rebate Price
                var rebate = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtRebate"));
                rebate.Text = rebatePrice.ToString();
                rebate.KeyIn(KeyboardInput.SpecialKeys.RETURN);
            }

            if (daystopaytxt.Text == "45" || string.IsNullOrEmpty(daystopaytxt.Text))
            {
            }
            else
            {
                // Days to Pay - Default 45.
                var days = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDaysPay"));
                days.Text = daystoPay.ToString();
                days.KeyIn(KeyboardInput.SpecialKeys.RETURN);
            }
        }

        private void FillTestDate()
        {
            var lastName = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerLastName"));
            lastName.Text = "Smith";

            var middleName = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerMiddleName"));
            middleName.Text = "Anthony";

            var Address = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerAddress"));
            Address.Text = "1 Main Street";

            var city = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerCity"));
            city.Text = "New York";

            var state = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("cboCustomerState"));
            state.Click();
            state.Select(6); 

            var zipcode = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerZip"));
            zipcode.Text = "12345";

            var PhoneZero = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerHomePhoneArea"));
            var PhoneOne = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerHomePhonePart1"));
            var PhoneTwo = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtCustomerHomePhonePart2"));

            PhoneZero.Text = "845";
            PhoneOne.Text = "123";
            PhoneTwo.Text = "3456";

            var submit = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
            submit.Click();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Stop(); // Stop Fiddler Core when closing form so it doesn't mess your connection up.

            //TODO: Fix this with conflicting with CreateObjectGraph()
            var menuList = new Menu();
            menuList.Show();
            this.Dispose();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (loaded)
            {
              loader.Close();
            }

            comboBox1.Items.AddRange(File.ReadAllLines(Environment.CurrentDirectory + @"\providers.txt"));
            DealType = _mainWindow.Name;


            dealTypetxt.Text = DealType.Trim();


            var materialSkinManager = MaterialSkinManager.Instance;

            switch (Settings.Default.color)
            {
                case 0:
                    materialSkinManager.AddFormToManage(this);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.LightBlue400, Primary.LightBlue400,
                        Primary.LightBlue400, Accent.LightBlue700, TextShade.WHITE);

                    break;
                case 1:
                    materialSkinManager.AddFormToManage(this);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Red700, Primary.Red700, Primary.Red700,
                        Accent.Red700, TextShade.WHITE);

                    break;
                case 2:
                    materialSkinManager.AddFormToManage(this);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Orange400, Primary.Orange400,
                        Primary.Orange400, Accent.Orange400, TextShade.WHITE);

                    break;
                case 3:
                    materialSkinManager.AddFormToManage(this);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Purple400, Primary.Purple400,
                        Primary.Purple400, Accent.Purple400, TextShade.WHITE);

                    break;
                case 4:
                    materialSkinManager.AddFormToManage(this);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Green400, Primary.Green400,
                        Primary.Green400, Accent.Green400, TextShade.WHITE);

                    break;
                case 5:
                    materialSkinManager.AddFormToManage(this);
                    materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                    materialSkinManager.ColorScheme = new ColorScheme(Primary.Yellow400, Primary.Yellow400,
                        Primary.Yellow400, Accent.Yellow400, TextShade.WHITE);

                    break;
            }
        }

        private void GetFilePath()
        {
            var processes = Process.GetProcessesByName("MainMenu");
            foreach (var process in processes) filepath = process.MainModule.FileName;
        }

        /// <summary>
        ///     Checks to see if there was an error or no rates returned. If no error is retuned it will return "false" to tell the
        ///     application to proceed to the next step.
        /// </summary>
        /// <returns></returns>
        private bool GrabErrorOrExucute()
        {
            using (CoreAppXmlConfiguration.Instance.ApplyTemporarySetting(s => s.BusyTimeout = 500))
            {
                if (_mainWindow.Exists(SearchCriteria.ByAutomationId("65535")))
                {
                    var error = _mainWindow.Get(
                        SearchCriteria.ByAutomationId(
                            "65535")); // This runs more than once for some reason. 

                    MessageBox.Show("Error has been thrown while getting rates, it will be displayed now...",
                        "Error While Getting Rates", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                    var result = MessageBox.Show(error.Name, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                    var okbtn = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("2"));
                    _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("2")).RaiseClickEvent();

                    Stop();
                    if (result == DialogResult.OK) CloseandReset(1);

                    return true;
                }

                return false;
            }
        }

        /// <summary>
        ///     Pulls rate from rating table.
        /// </summary>
        private void GrabRate()
        {
            if (_mainWindow.Exists<Table>(SearchCriteria.ByAutomationId("ratesDataGridView")))
            {
                var rateTable = _mainWindow.Get<Table>(SearchCriteria.ByAutomationId("ratesDataGridView"));
                rateValues.Clear();
                SendKeys.SendWait("{TAB}");
                var rowCount = rateTable.Rows.Count;
                var random = new Random();
                var randomCell = random.Next(1, rowCount);
                var rows = rateTable.Rows.SelectMany(s => s.Cells);
                var cellCounter = 0;
                var cellRow = new List<string>();

                foreach (var rowCellValue in rows)
                {
                    if (cellCounter == rateTable.Rows[0].Cells.Count)
                    {
                        rateValues.Add(string.Join(",", cellRow.ToArray()));
                        cellRow.Clear();
                        cellCounter = 0;
                    }

                    cellRow.Add(rowCellValue.Value.ToString());
                    cellCounter++;

                    // TODO: Fix to grab last item
                }

                var selectedCell = rateTable.Rows[randomCell].Cells.Select(s => s.Value);
                selectedRate = string.Join(",", selectedCell);

                for (var i = 0; i <= randomCell; i++) SendKeys.SendWait("{ENTER}");

                var ok = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("btnOK"));
                ok.Click();
                if (_mainWindow.Exists<Button>(SearchCriteria.ByAutomationId("6")))
                {
                    var yes = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("6"));
                    yes.Click();
                }
            }
            else
            {
                MessageBox.Show("Windows Automation could not find the Rate Table, this could be because it is not displayed yet or has encountered an error.");
            }
        }

        private void installVertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InstallCertificate();
        }

        // Return a string representing ListView column headers.
        private string ListViewColumnHeaderHtml(ListView lvw)
        {
            // Display the column headers.
            var txt = "  <tr>";
            foreach (ColumnHeader col in lvw.Columns)

                // Display this column header.
                txt += "<th bgcolor=\"#CCFFFF\"" + "width=\"" + col.Width + "\" " + "align=\"" + col.TextAlign + "\">" +
                       col.Text + "</th>";

            txt += "</tr>\n";
            return txt;
        }

        // Return the HTML text representing this item's row.
        private string ListViewItemHtml(ListViewItem item)
        {
            var txt = "  <tr>";
            var lvw = item.ListView;
            for (var i = 0; i < item.SubItems.Count; i++)
                txt += "<td " + "bgcolor=" + "'" + item.BackColor.Name + "'" + " align=\"" + lvw.Columns[i].TextAlign +
                       "\">" + ReplaceBrackets(item.SubItems[i].Text) + "</td>";

            txt += "</tr>\n";
            return txt;
        }

        /// <summary>
        ///     Return an HTML table showing the ListView's contents.
        /// </summary>
        /// <param name="lvw">The listview</param>
        /// <param name="border">The border.</param>
        /// <param name="cell_spacing">The cell spacing.</param>
        /// <param name="cell_padding">The cell padding.</param>
        /// <returns></returns>
        private string ListViewToHtmlTable(ListView lvw, int border, int cell_spacing, int cell_padding)
        {
            // Open the <table> element.
            var txt = "<table class='table table-bordered' style='display: inline'" + "border=\"" + border + "\" " +
                      "cellspacing=\"" + cell_spacing + "\" " + "cellpadding=\"" + cell_padding + "\">\n";

            // See how many columns there are.
            var num_cols = lvw.Columns.Count;

            // See if there are any non-grouped items.
            var have_non_grouped_items = false;
            foreach (ListViewItem item in lvw.Items)
                if (item.Group == null)
                {
                    have_non_grouped_items = true;
                    break;
                }

            // Display non-grouped items.
            if (have_non_grouped_items)
            {
                // Display the column headers.
                txt += ListViewColumnHeaderHtml(lvw);

                // Display the non-grouped items.
                foreach (ListViewItem item in lvw.Items)
                    if (item.Group == null)
                        txt += ListViewItemHtml(item);
            }

            // Process the groups.
            foreach (ListViewGroup grp in lvw.Groups)
            {
                // Display the header.
                txt += "  <tr><th " + "colspan=\"" + num_cols + "\" " + "align=\"" + grp.HeaderAlignment + "\" " +
                       "bgcolor=\"LightGreen\">" + grp.Header + "</th></tr>\n";

                // Display the column headers.
                txt += ListViewColumnHeaderHtml(lvw);

                // Display the items in the group.
                foreach (ListViewItem item in grp.Items) txt += ListViewItemHtml(item);
            }

            txt += "</table>\n";
            return txt;
        }


        /// <summary>
        ///     This should be used after you pull a DMS Deal. This will gather all fields and items from the menu and get them
        ///     ready to compare to the DMS values.
        /// </summary>
        private void MenuItemsAfterDms()
        {
            var nameMapping = new Dictionary<string, int>
            {
                {"prodOne", 1},
                {"prodTwo", 2},
                {"prodThree", 3},
                {"prodFour", 4},
                {"prodFive", 5},
                {"prodSix", 6},
                {"prodSeven", 7},
                {"prodEight", 8}
            };

            var checkMapping = new Dictionary<string, int>
            {
                {"prodBox1", 1},
                {"prodBox2", 2},
                {"prodBox3", 3},
                {"prodBox4", 4},
                {"prodBox5", 5},
                {"prodBox6", 6},
                {"prodBox7", 7},
                {"prodBox8", 8}
            };

            var priceMapping = new Dictionary<string, int>
            {
                {"txtProdBox1", 1},
                {"txtProdBox2", 2},
                {"txtProdBox3", 3},
                {"txtProdBox4", 4},
                {"txtProdBox5", 5},
                {"txtProdBox6", 6},
                {"txtProdBox7", 7},
                {"txtProdBox8", 8}
            };

            try
            {
                var txtName = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtName")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtManager = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("txtManager"))
                    .SelectedItemText;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtSalesMan = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("txtSalesMan"))
                    .SelectedItemText;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtStockNum = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtStockNum")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtPrice = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtPrice")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var taxedFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblTaxedFees")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var untaxedFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblUntaxedFees")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var totalFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblTotalFees")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtTrade = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtTrade")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtPayoff = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtPayoff")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtDown = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDown")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtServiceTerm = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtServiceTerm")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtServiceMiles = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtServiceMiles")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var serviceterm = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtServiceTerm")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var cbDeduct = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("1001")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var txtSalesTax = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtSalesTax")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var finalpay = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("finalPay")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var term = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("termBox")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var aprbox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprBox")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var discount = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("discountBox")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            try
            {
                var quickpay = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("quickPay")).Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            // Get Payment info ->
            // Not sure why but the first element is last. So like 2,3,4,5,1
            var paymentPanel = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("panel2"));
            var paymentCheckBoxes = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.CheckBox))
                .Cast<CheckBox>()
                .ToList();

            var planNames = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
            var prices = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.Edit))
                .Cast<TextBox>()
                .ToList();

            var orderedNames = planNames.OrderBy(x => nameMapping[x.Id]).ToList();
            var orderedCheckboxs = paymentCheckBoxes.OrderBy(x => checkMapping[x.Id]).ToList();
            var orderedPrices = prices.OrderBy(x => priceMapping[x.Id]).ToList();
            for (var i = 0; i < orderedNames.Count; i++)
            {
                // MessageBox.Show(orderedNames[i].Name + " " + orderedPrices[i].Text + " " + orderedCheckboxs[i].Checked);
            }

            // Fees - Grab Fee Name, Price, Taxed, Upfront and Enabled --------------------->
            var itemFees = new List<string>();
            var itemCheckBox = new List<string>();
            var options = new List<string> {"Enabled", "Taxed", "Upfront"};

            var lblFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblFees"));
            SendKeys.SendWait("{TAB}"); // Needs this or the label won't click for some reason.
            lblFees.Click();

            var _feeWindow = _mainWindow.ModalWindow("Fees");
            var flpRows = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("flpRows"));
            var fees = _feeWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
            var fees_original = _feeWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
            var fee_checkbox = _feeWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.CheckBox)).ToList();

            // Remove all default values.
            fees.RemoveAll(s =>
                s.Name.ToString() == "Enabled" || s.Name.ToString() == "Description" || s.Name.ToString() == "Amount" ||
                s.Name.ToString() == "Taxed" || s.Name.ToString() == "Upfront");

            foreach (var item_name in fees)
            {
                var price = _feeWindow.Get<TextBox>(SearchCriteria.ByText(item_name.Name)).Text;
                itemFees.Add(item_name.Name + " / " + price); // CANNOT PUT THESE IN TWO ITEMS (PRICE / NAME)
            }

            // I tested this with 1,2,3,4,5,6 fees. The logic works, don't ask me how but it does.
            if (form2.IsDisposed) form2 = new Form2();

            form2.Show(this);

            var interation = 0;
            var keeptrack = 0;
            var checkList = new List<string>();
            foreach (CheckBox checkBox in fee_checkbox)
            {
                if (interation % 3 == 0) form2.listView1.Items.Add(itemFees[keeptrack / 3]);
                if (interation == 3) interation = 0;
                checkList.Add(options[interation] + " = " + checkBox.Checked);
                if (checkList.Count == 3)
                {
                    form2.listView1.Items.Add(checkList[0] + " , " + checkList[1] + " , " + checkList[2]);
                    checkList.Clear();
                }

                interation++;
                keeptrack++;
            }
        }

        private void OpenMenu()
        {
            var menu_panel = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("menu"));
            menu_panel.Click();
        }

        private void PullProductInfo()
        {
            var xDocument = XDocument.Load(Path.GetDirectoryName(filepath) + @"\ProductsV2.xml");
            var elements = from e_items in xDocument.Descendants("ProductEntry")
                select new
                {
                    isTaxable = e_items.Element("Taxable").Value,
                    isOverridden = e_items.Element("OverrideTaxRate").Value
                };
        }

        private void rateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            GrabRate();
        }

        private void reHookApplicationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetupAutomation();
        }

        /// <summary>
        ///     Sends the report that was ran via email to support.
        /// </summary>
        private void SendReport()
        {
            // TODO: Email password not in plain text.
            var fromAddress = new MailAddress("frua134@gmail.com", "OptionSoft - Automated Menu Report");
            var toAddress = new MailAddress("frua134@gmail.com", "To Name");
            const string fromPassword = "--";
            const string subject = "Subject";
            const string body = "TEST";

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            using (var message = new MailMessage(fromAddress, toAddress) {Subject = subject, Body = body})
            {
                var attachment = new Attachment(Environment.CurrentDirectory + @"\RatingSnapshot.html");
                message.Attachments.Add(attachment);

                smtp.Send(message);
            }
        }

        /// <summary>
        ///     USed to check the QA Settings state of the menu on form loadup and display them in the context menu.
        /// </summary>
        private void SettingsState()
        {
            var currentstate = string.Empty;

            // OTI Server ----------------------------------------------
            var xDocument = XDocument.Load(Path.GetDirectoryName(filepath) + @"\NewSettings.xml");
            var elements = from e_items in xDocument.Descendants("Settings")
                where e_items.Element("SettingName").Value == "OTIServerQAMode"
                select e_items;

            foreach (var itemElement in elements) currentstate = itemElement.Element("SettingValue").Value;

            OTIServerMenuStrip.Text =
                currentstate == "True" ? " OTI Server QA Mode: True" : " OTI Server QA Mode: False";

            // Rating Server ---------------------------------------------
            elements = from e_items in xDocument.Descendants("Settings")
                where e_items.Element("SettingName").Value == "RatingServerQAMode"
                select e_items;

            foreach (var itemElement in elements) currentstate = itemElement.Element("SettingValue").Value;

            RatingServerMenuStrip.Text = currentstate == "True"
                ? " Rating Server QA Mode: True"
                : " Rating Server QA Mode: False";
        }

        private void SetupAutomation()
        {
            try
            {
                var processes = Process.GetProcessesByName("MainMenu");
                foreach (var process in processes)
                {
                    id = process.Id;
                    menu_title = process.MainWindowTitle;
                }

                CoreAppXmlConfiguration.Instance.BusyTimeout = 40000;

                // Attach to Menu based on ID from above.
                _application = Application.Attach(id);
                _mainWindow = _application.GetWindow(SearchCriteria.ByText(menu_title), InitializeOption.NoCache);
                loaded = true;
                GetFilePath();
                dealTypetxt.Text = DealType;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not hook application, is the menu open? ");
                System.Windows.Forms.Application.Exit();
            }
        }

        /// <summary>
        ///     Starts up a new session to capture request data via the Fiddler DLL.
        /// </summary>
        private void Start()
        {
            if (FiddlerApplication.IsStarted())
            {
            }
            else
            {
                CaptureConfiguration.ProcessId = id;

                // CaptureConfiguration.CaptureDomain = 
                FiddlerApplication.AfterSessionComplete += Fiddler_AfterSession;
                FiddlerApplication.Startup(8888, true, true, true);
            }
        }

        private void startPacketSniffingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Start();

        }

        /// <summary>
        ///     Stops all Fiddler Sessions. If you're getting proxy error's on your browser, use this method to stop the proxy
        ///     server.
        /// </summary>
        private void Stop()
        {
            FiddlerApplication.AfterSessionComplete -= Fiddler_AfterSession;

            if (FiddlerApplication.IsStarted()) FiddlerApplication.Shutdown();
        }

        private void stopPacketSniffingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void ThreadSafeForm_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            status.Text = "Complete";
        }

        private void uninstallCertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UninstallCertificate();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            Contract();
        }

        private void OpenMenuButton_Click(object sender, EventArgs e)
        {
            OpenMenu();
        }

        private void serverQAModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Quick and dirty method to change settings in FI menu, requires restart.
            var filepath = string.Empty;

            var processes = Process.GetProcessesByName("MainMenu");
            foreach (var process in processes) filepath = process.MainModule.FileName;

            var xDocument = XDocument.Load(Path.GetDirectoryName(filepath) + @"\NewSettings.xml");
            var elements = from e_items in xDocument.Descendants("Settings")
                where e_items.Element("SettingName").Value == "OTIServerQAMode"
                select e_items;

            foreach (var itemElement in elements)
                itemElement.SetElementValue("SettingValue",
                    OTIServerMenuStrip.Text.Contains("True") ? "False" : "True");

            xDocument.Save(Path.GetDirectoryName(filepath) + @"\NewSettings.xml");
            SettingsState(); // Update context menu by by pulling the most current value from XML settings.

            MessageBox.Show(
                "This settings change require a menu restart to work correctly, the menu will restart now...",
                "Restart Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            RestartApplication();
        }

        private void ratingServerQAModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Quick and dirty method to change settings in FI menu, requires restart.
            var filepath = string.Empty;

            var processes = Process.GetProcessesByName("MainMenu");
            foreach (var process in processes) filepath = process.MainModule.FileName;

            var xDocument = XDocument.Load(Path.GetDirectoryName(filepath) + @"\NewSettings.xml");
            var elements = from e_items in xDocument.Descendants("Settings")
                where e_items.Element("SettingName").Value == "RatingServerQAMode"
                select e_items;

            foreach (var itemElement in elements)
                if (RatingServerMenuStrip.Text.Contains("True"))
                    itemElement.SetElementValue("SettingValue", "False");
                else
                    itemElement.SetElementValue("SettingValue", "True");

            xDocument.Save(Path.GetDirectoryName(filepath) + @"\NewSettings.xml");
            SettingsState(); // Update context menu by by pulling the most current value from XML settings.
            MessageBox.Show(
                "This settings change require a menu restart to work correctly, the menu will restart now...",
                "Restart Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            RestartApplication();
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form2 = new Form2();
            form2.Show();
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.color = 0;
            Settings.Default.Save();
            System.Windows.Forms.Application.Restart();
        }

        private void redToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Settings.Default.color = 1;
            Settings.Default.Save();
            System.Windows.Forms.Application.Restart();
        }

        private void orangeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.color = 2;
            Settings.Default.Save();
            System.Windows.Forms.Application.Restart();
        }

        private void purpleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.color = 3;
            Settings.Default.Save();
            System.Windows.Forms.Application.Restart();
        }

        private void greenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.color = 4;
            Settings.Default.Save();
            System.Windows.Forms.Application.Restart();
        }

        private void yellowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.color = 5;
            Settings.Default.Save();
            System.Windows.Forms.Application.Restart();
        }

        private void showXMLTESTToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(XMLRequest)) return;

            File.WriteAllText(Environment.CurrentDirectory + @"\request.xml", XMLRequest, Encoding.UTF8);
            File.WriteAllLines(Environment.CurrentDirectory + @"\request.xml",
                File.ReadAllLines(Environment.CurrentDirectory + @"\request.xml").Skip(8));

            dms_check.Enabled = false;
        }

        private void grabItemsTESTToolStripMenuItem_Click(object sender, EventArgs e)
        {
          
        }

        private void dateTESTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //List<string> buttonoptions = new List<string>
            //{
            //    "btnFullPay",
            //    "btnSelectionsPrint",
            //    "btnGroupOption1",
            //    "btnGroupOption2",
            //    "btnGroupOption3",
            //    "btnGroupOption4",
            //    "btnRecap",
            //    "btnFinalPrint"
            //};

            //foreach (string s in buttonoptions)
            //{
            //    var button = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId(s));
            //    button.Click();

            //    Thread.Sleep(400);

            //    this.Hide();
            //    var image = ScreenCapture.CaptureDesktop();
            //    image.Save(@"C:\Users\frua\Desktop\" + s + ".jpg", ImageFormat.Jpeg);

            //    var base64Image = Convert.ToBase64String(File.ReadAllBytes(@"C:\Users\frua\Desktop\" + s + ".jpg"));
            //    string converted = base64Image.Replace('-', '+');

            //    File.WriteAllText(@"C:\Users\frua\Desktop\" + s +".txt",  converted);

            //    File.Delete(@"C:\Users\frua\Desktop\" + s + ".jpg");

            //    this.Show();
            //    _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("Close")).Click();

            //}


            var img = Image.FromStream(new MemoryStream(Convert.FromBase64String(@"C:\Users\frua\Desktop\btnRecap.txt")));
            img.Save(@"C:\Users\frua\Desktop\base64convert.jpg",ImageFormat.Jpeg);

        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            var senderComboBox = (System.Windows.Forms.ComboBox) sender;
            var width = senderComboBox.DropDownWidth;
            var g = senderComboBox.CreateGraphics();
            var font = senderComboBox.Font;

            var vertScrollBarWidth = senderComboBox.Items.Count > senderComboBox.MaxDropDownItems
                ? SystemInformation.VerticalScrollBarWidth
                : 0;

            var itemsList = senderComboBox.Items.Cast<object>().Select(item => item.ToString());

            foreach (var s in itemsList)
            {
                var newWidth = (int) g.MeasureString(s, font).Width + vertScrollBarWidth;

                if (width < newWidth) width = newWidth;
            }

            senderComboBox.DropDownWidth = width;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            comboBox1.Items.Add(addProvider.Text);
            File.AppendAllText(Environment.CurrentDirectory + @"\providers.txt",
                Environment.NewLine + addProvider.Text);

            MessageBox.Show(addProvider.Text + " Added to list.");
        }

        private void addRatingToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void editRatingsListToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(comboBox1.Text) == false && string.IsNullOrEmpty(vintxt.Text) == false)
            {
                ListViewItem item = new ListViewItem();
                item.Text = comboBox1.Text;
                item.SubItems.Add(ClientNametxt.Text);
                item.SubItems.Add(SellingPricetxt.Text);
                item.SubItems.Add(TradeIntxt.Text);
                item.SubItems.Add(Payofftxt.Text);
                item.SubItems.Add(downpaytxt.Text);
                item.SubItems.Add(Rebatetxt.Text);
                item.SubItems.Add(daystopaytxt.Text);
                item.SubItems.Add(vintxt.Text);
                item.SubItems.Add(odomttx.Text);
                listView1.Items.Add(item);
            }
            else
            {
                MessageBox.Show("VIN or Rating Provider field cannot be left blank.");
            }

        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            checkBox1.Location = new Point(checkBox1.Location.X, checkBox1.Location.Y + 30);
            newRadio.Location = new Point(newRadio.Location.X, newRadio.Location.Y + 30);
            oldRadio.Location = new Point(oldRadio.Location.X, oldRadio.Location.Y + 30);

            addProvider.Visible = true;
            addButton.Visible = true;

            materialRaisedButton2.Enabled = false;
        }

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(Environment.CurrentDirectory + @"\providers.txt");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}