﻿namespace FI_Automation
{
    partial class DmsSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientname = new MaterialSkin.Controls.MaterialCheckBox();
            this.manager = new MaterialSkin.Controls.MaterialCheckBox();
            this.salesperson = new MaterialSkin.Controls.MaterialCheckBox();
            this.stocknum = new MaterialSkin.Controls.MaterialCheckBox();
            this.sellingprice = new MaterialSkin.Controls.MaterialCheckBox();
            this.tradeamount = new MaterialSkin.Controls.MaterialCheckBox();
            this.payoff = new MaterialSkin.Controls.MaterialCheckBox();
            this.downpay = new MaterialSkin.Controls.MaterialCheckBox();
            this.rebate = new MaterialSkin.Controls.MaterialCheckBox();
            this.daystopay = new MaterialSkin.Controls.MaterialCheckBox();
            this.salestax = new MaterialSkin.Controls.MaterialCheckBox();
            this.fees = new MaterialSkin.Controls.MaterialCheckBox();
            this.products = new MaterialSkin.Controls.MaterialCheckBox();
            this.blank = new MaterialSkin.Controls.MaterialCheckBox();
            this.payment = new MaterialSkin.Controls.MaterialCheckBox();
            this.apr = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // clientname
            // 
            this.clientname.AutoSize = true;
            this.clientname.Depth = 0;
            this.clientname.Font = new System.Drawing.Font("Roboto", 10F);
            this.clientname.Location = new System.Drawing.Point(22, 144);
            this.clientname.Margin = new System.Windows.Forms.Padding(0);
            this.clientname.MouseLocation = new System.Drawing.Point(-1, -1);
            this.clientname.MouseState = MaterialSkin.MouseState.HOVER;
            this.clientname.Name = "clientname";
            this.clientname.Ripple = true;
            this.clientname.Size = new System.Drawing.Size(105, 30);
            this.clientname.TabIndex = 0;
            this.clientname.Text = "Client Name";
            this.clientname.UseVisualStyleBackColor = true;
            this.clientname.CheckedChanged += new System.EventHandler(this.clientname_CheckedChanged);
            // 
            // manager
            // 
            this.manager.AutoSize = true;
            this.manager.Depth = 0;
            this.manager.Font = new System.Drawing.Font("Roboto", 10F);
            this.manager.Location = new System.Drawing.Point(22, 174);
            this.manager.Margin = new System.Windows.Forms.Padding(0);
            this.manager.MouseLocation = new System.Drawing.Point(-1, -1);
            this.manager.MouseState = MaterialSkin.MouseState.HOVER;
            this.manager.Name = "manager";
            this.manager.Ripple = true;
            this.manager.Size = new System.Drawing.Size(84, 30);
            this.manager.TabIndex = 1;
            this.manager.Text = "Manager";
            this.manager.UseVisualStyleBackColor = true;
            this.manager.CheckedChanged += new System.EventHandler(this.manager_CheckedChanged);
            // 
            // salesperson
            // 
            this.salesperson.AutoSize = true;
            this.salesperson.Depth = 0;
            this.salesperson.Font = new System.Drawing.Font("Roboto", 10F);
            this.salesperson.Location = new System.Drawing.Point(22, 204);
            this.salesperson.Margin = new System.Windows.Forms.Padding(0);
            this.salesperson.MouseLocation = new System.Drawing.Point(-1, -1);
            this.salesperson.MouseState = MaterialSkin.MouseState.HOVER;
            this.salesperson.Name = "salesperson";
            this.salesperson.Ripple = true;
            this.salesperson.Size = new System.Drawing.Size(111, 30);
            this.salesperson.TabIndex = 2;
            this.salesperson.Text = "Sales Person";
            this.salesperson.UseVisualStyleBackColor = true;
            this.salesperson.CheckedChanged += new System.EventHandler(this.salesperson_CheckedChanged);
            // 
            // stocknum
            // 
            this.stocknum.AutoSize = true;
            this.stocknum.Depth = 0;
            this.stocknum.Font = new System.Drawing.Font("Roboto", 10F);
            this.stocknum.Location = new System.Drawing.Point(22, 234);
            this.stocknum.Margin = new System.Windows.Forms.Padding(0);
            this.stocknum.MouseLocation = new System.Drawing.Point(-1, -1);
            this.stocknum.MouseState = MaterialSkin.MouseState.HOVER;
            this.stocknum.Name = "stocknum";
            this.stocknum.Ripple = true;
            this.stocknum.Size = new System.Drawing.Size(77, 30);
            this.stocknum.TabIndex = 3;
            this.stocknum.Text = "Stock #";
            this.stocknum.UseVisualStyleBackColor = true;
            this.stocknum.CheckedChanged += new System.EventHandler(this.stocknum_CheckedChanged);
            // 
            // sellingprice
            // 
            this.sellingprice.AutoSize = true;
            this.sellingprice.Depth = 0;
            this.sellingprice.Font = new System.Drawing.Font("Roboto", 10F);
            this.sellingprice.Location = new System.Drawing.Point(22, 264);
            this.sellingprice.Margin = new System.Windows.Forms.Padding(0);
            this.sellingprice.MouseLocation = new System.Drawing.Point(-1, -1);
            this.sellingprice.MouseState = MaterialSkin.MouseState.HOVER;
            this.sellingprice.Name = "sellingprice";
            this.sellingprice.Ripple = true;
            this.sellingprice.Size = new System.Drawing.Size(107, 30);
            this.sellingprice.TabIndex = 4;
            this.sellingprice.Text = "Selling Price";
            this.sellingprice.UseVisualStyleBackColor = true;
            this.sellingprice.CheckedChanged += new System.EventHandler(this.sellingprice_CheckedChanged);
            // 
            // tradeamount
            // 
            this.tradeamount.AutoSize = true;
            this.tradeamount.Depth = 0;
            this.tradeamount.Font = new System.Drawing.Font("Roboto", 10F);
            this.tradeamount.Location = new System.Drawing.Point(22, 294);
            this.tradeamount.Margin = new System.Windows.Forms.Padding(0);
            this.tradeamount.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tradeamount.MouseState = MaterialSkin.MouseState.HOVER;
            this.tradeamount.Name = "tradeamount";
            this.tradeamount.Ripple = true;
            this.tradeamount.Size = new System.Drawing.Size(118, 30);
            this.tradeamount.TabIndex = 5;
            this.tradeamount.Text = "Trade Amount";
            this.tradeamount.UseVisualStyleBackColor = true;
            this.tradeamount.CheckedChanged += new System.EventHandler(this.tradeamount_CheckedChanged);
            // 
            // payoff
            // 
            this.payoff.AutoSize = true;
            this.payoff.Depth = 0;
            this.payoff.Font = new System.Drawing.Font("Roboto", 10F);
            this.payoff.Location = new System.Drawing.Point(22, 324);
            this.payoff.Margin = new System.Windows.Forms.Padding(0);
            this.payoff.MouseLocation = new System.Drawing.Point(-1, -1);
            this.payoff.MouseState = MaterialSkin.MouseState.HOVER;
            this.payoff.Name = "payoff";
            this.payoff.Ripple = true;
            this.payoff.Size = new System.Drawing.Size(70, 30);
            this.payoff.TabIndex = 6;
            this.payoff.Text = "Payoff";
            this.payoff.UseVisualStyleBackColor = true;
            this.payoff.CheckedChanged += new System.EventHandler(this.payoff_CheckedChanged);
            // 
            // downpay
            // 
            this.downpay.AutoSize = true;
            this.downpay.Depth = 0;
            this.downpay.Font = new System.Drawing.Font("Roboto", 10F);
            this.downpay.Location = new System.Drawing.Point(157, 324);
            this.downpay.Margin = new System.Windows.Forms.Padding(0);
            this.downpay.MouseLocation = new System.Drawing.Point(-1, -1);
            this.downpay.MouseState = MaterialSkin.MouseState.HOVER;
            this.downpay.Name = "downpay";
            this.downpay.Ripple = true;
            this.downpay.Size = new System.Drawing.Size(118, 30);
            this.downpay.TabIndex = 7;
            this.downpay.Text = "Downpayment";
            this.downpay.UseVisualStyleBackColor = true;
            this.downpay.CheckedChanged += new System.EventHandler(this.downpay_CheckedChanged);
            // 
            // rebate
            // 
            this.rebate.AutoSize = true;
            this.rebate.Depth = 0;
            this.rebate.Font = new System.Drawing.Font("Roboto", 10F);
            this.rebate.Location = new System.Drawing.Point(157, 144);
            this.rebate.Margin = new System.Windows.Forms.Padding(0);
            this.rebate.MouseLocation = new System.Drawing.Point(-1, -1);
            this.rebate.MouseState = MaterialSkin.MouseState.HOVER;
            this.rebate.Name = "rebate";
            this.rebate.Ripple = true;
            this.rebate.Size = new System.Drawing.Size(73, 30);
            this.rebate.TabIndex = 8;
            this.rebate.Text = "Rebate";
            this.rebate.UseVisualStyleBackColor = true;
            this.rebate.CheckedChanged += new System.EventHandler(this.rebate_CheckedChanged);
            // 
            // daystopay
            // 
            this.daystopay.AutoSize = true;
            this.daystopay.Depth = 0;
            this.daystopay.Font = new System.Drawing.Font("Roboto", 10F);
            this.daystopay.Location = new System.Drawing.Point(157, 174);
            this.daystopay.Margin = new System.Windows.Forms.Padding(0);
            this.daystopay.MouseLocation = new System.Drawing.Point(-1, -1);
            this.daystopay.MouseState = MaterialSkin.MouseState.HOVER;
            this.daystopay.Name = "daystopay";
            this.daystopay.Ripple = true;
            this.daystopay.Size = new System.Drawing.Size(102, 30);
            this.daystopay.TabIndex = 9;
            this.daystopay.Text = "Days to Pay";
            this.daystopay.UseVisualStyleBackColor = true;
            this.daystopay.CheckedChanged += new System.EventHandler(this.daystopay_CheckedChanged);
            // 
            // salestax
            // 
            this.salestax.AutoSize = true;
            this.salestax.Depth = 0;
            this.salestax.Font = new System.Drawing.Font("Roboto", 10F);
            this.salestax.Location = new System.Drawing.Point(157, 204);
            this.salestax.Margin = new System.Windows.Forms.Padding(0);
            this.salestax.MouseLocation = new System.Drawing.Point(-1, -1);
            this.salestax.MouseState = MaterialSkin.MouseState.HOVER;
            this.salestax.Name = "salestax";
            this.salestax.Ripple = true;
            this.salestax.Size = new System.Drawing.Size(90, 30);
            this.salestax.TabIndex = 10;
            this.salestax.Text = "Sales Tax";
            this.salestax.UseVisualStyleBackColor = true;
            this.salestax.CheckedChanged += new System.EventHandler(this.salestax_CheckedChanged);
            // 
            // fees
            // 
            this.fees.AutoSize = true;
            this.fees.Depth = 0;
            this.fees.Font = new System.Drawing.Font("Roboto", 10F);
            this.fees.Location = new System.Drawing.Point(157, 234);
            this.fees.Margin = new System.Windows.Forms.Padding(0);
            this.fees.MouseLocation = new System.Drawing.Point(-1, -1);
            this.fees.MouseState = MaterialSkin.MouseState.HOVER;
            this.fees.Name = "fees";
            this.fees.Ripple = true;
            this.fees.Size = new System.Drawing.Size(59, 30);
            this.fees.TabIndex = 11;
            this.fees.Text = "Fees";
            this.fees.UseVisualStyleBackColor = true;
            this.fees.CheckedChanged += new System.EventHandler(this.fees_CheckedChanged);
            // 
            // products
            // 
            this.products.AutoSize = true;
            this.products.Depth = 0;
            this.products.Font = new System.Drawing.Font("Roboto", 10F);
            this.products.Location = new System.Drawing.Point(157, 264);
            this.products.Margin = new System.Windows.Forms.Padding(0);
            this.products.MouseLocation = new System.Drawing.Point(-1, -1);
            this.products.MouseState = MaterialSkin.MouseState.HOVER;
            this.products.Name = "products";
            this.products.Ripple = true;
            this.products.Size = new System.Drawing.Size(86, 30);
            this.products.TabIndex = 12;
            this.products.Text = "Products";
            this.products.UseVisualStyleBackColor = true;
            this.products.CheckedChanged += new System.EventHandler(this.products_CheckedChanged);
            // 
            // blank
            // 
            this.blank.AutoSize = true;
            this.blank.Depth = 0;
            this.blank.Font = new System.Drawing.Font("Roboto", 10F);
            this.blank.Location = new System.Drawing.Point(157, 294);
            this.blank.Margin = new System.Windows.Forms.Padding(0);
            this.blank.MouseLocation = new System.Drawing.Point(-1, -1);
            this.blank.MouseState = MaterialSkin.MouseState.HOVER;
            this.blank.Name = "blank";
            this.blank.Ripple = true;
            this.blank.Size = new System.Drawing.Size(105, 30);
            this.blank.TabIndex = 13;
            this.blank.Text = "Client Name";
            this.blank.UseVisualStyleBackColor = true;
            this.blank.CheckedChanged += new System.EventHandler(this.blank_CheckedChanged);
            // 
            // payment
            // 
            this.payment.AutoSize = true;
            this.payment.Depth = 0;
            this.payment.Font = new System.Drawing.Font("Roboto", 10F);
            this.payment.Location = new System.Drawing.Point(22, 354);
            this.payment.Margin = new System.Windows.Forms.Padding(0);
            this.payment.MouseLocation = new System.Drawing.Point(-1, -1);
            this.payment.MouseState = MaterialSkin.MouseState.HOVER;
            this.payment.Name = "payment";
            this.payment.Ripple = true;
            this.payment.Size = new System.Drawing.Size(84, 30);
            this.payment.TabIndex = 14;
            this.payment.Text = "Payment";
            this.payment.UseVisualStyleBackColor = true;
            this.payment.CheckedChanged += new System.EventHandler(this.payment_CheckedChanged);
            // 
            // apr
            // 
            this.apr.AutoSize = true;
            this.apr.Depth = 0;
            this.apr.Font = new System.Drawing.Font("Roboto", 10F);
            this.apr.Location = new System.Drawing.Point(157, 354);
            this.apr.Margin = new System.Windows.Forms.Padding(0);
            this.apr.MouseLocation = new System.Drawing.Point(-1, -1);
            this.apr.MouseState = MaterialSkin.MouseState.HOVER;
            this.apr.Name = "apr";
            this.apr.Ripple = true;
            this.apr.Size = new System.Drawing.Size(56, 30);
            this.apr.TabIndex = 15;
            this.apr.Text = "APR";
            this.apr.UseVisualStyleBackColor = true;
            this.apr.CheckedChanged += new System.EventHandler(this.apr_CheckedChanged);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(20, 75);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(163, 19);
            this.materialLabel1.TabIndex = 16;
            this.materialLabel1.Text = "DMS Info To Be Pulled:";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(24, 109);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(84, 23);
            this.materialRaisedButton1.TabIndex = 17;
            this.materialRaisedButton1.Text = "Check All";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            // 
            // DmsSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 498);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.apr);
            this.Controls.Add(this.payment);
            this.Controls.Add(this.blank);
            this.Controls.Add(this.products);
            this.Controls.Add(this.fees);
            this.Controls.Add(this.salestax);
            this.Controls.Add(this.daystopay);
            this.Controls.Add(this.rebate);
            this.Controls.Add(this.downpay);
            this.Controls.Add(this.payoff);
            this.Controls.Add(this.tradeamount);
            this.Controls.Add(this.sellingprice);
            this.Controls.Add(this.stocknum);
            this.Controls.Add(this.salesperson);
            this.Controls.Add(this.manager);
            this.Controls.Add(this.clientname);
            this.Name = "DmsSettings";
            this.Text = "DMS Configuration";
            this.Load += new System.EventHandler(this.DmsSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialCheckBox clientname;
        private MaterialSkin.Controls.MaterialCheckBox manager;
        private MaterialSkin.Controls.MaterialCheckBox salesperson;
        private MaterialSkin.Controls.MaterialCheckBox stocknum;
        private MaterialSkin.Controls.MaterialCheckBox sellingprice;
        private MaterialSkin.Controls.MaterialCheckBox tradeamount;
        private MaterialSkin.Controls.MaterialCheckBox payoff;
        private MaterialSkin.Controls.MaterialCheckBox downpay;
        private MaterialSkin.Controls.MaterialCheckBox rebate;
        private MaterialSkin.Controls.MaterialCheckBox daystopay;
        private MaterialSkin.Controls.MaterialCheckBox salestax;
        private MaterialSkin.Controls.MaterialCheckBox fees;
        private MaterialSkin.Controls.MaterialCheckBox products;
        private MaterialSkin.Controls.MaterialCheckBox blank;
        private MaterialSkin.Controls.MaterialCheckBox payment;
        private MaterialSkin.Controls.MaterialCheckBox apr;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
    }
}