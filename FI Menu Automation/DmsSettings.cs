﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin;
using MaterialSkin.Controls;

namespace FI_Automation
{
    public partial class DmsSettings : MaterialForm
    {
        public DmsSettings()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
        }

        private void LoadSettings()
        {
            if (Properties.Settings.Default.ClientName == "y") clientname.Checked = true;
            if (Properties.Settings.Default.Manager == "y") manager.Checked = true;
            if (Properties.Settings.Default.SalesPerson == "y") salesperson.Checked = true;
            if (Properties.Settings.Default.Stock == "y") stocknum.Checked = true;
            if (Properties.Settings.Default.SellingPrice == "y") sellingprice.Checked = true;
            if (Properties.Settings.Default.TradeAmount == "y") tradeamount.Checked = true;
            if (Properties.Settings.Default.Payoff == "y") payoff.Checked = true;
            if (Properties.Settings.Default.Payment == "y") payment.Checked = true;
            if (Properties.Settings.Default.Rebate == "y") rebate.Checked = true;
            if (Properties.Settings.Default.DaysToPay == "y") daystopay.Checked = true;
            if (Properties.Settings.Default.SalesTax == "y") salestax.Checked = true;
            if (Properties.Settings.Default.Fees == "y") fees.Checked = true;
            if (Properties.Settings.Default.Products == "y") products.Checked = true;
            if (Properties.Settings.Default.blank == "y") blank.Checked = true;
            if (Properties.Settings.Default.Downpay == "y") downpay.Checked = true;
            if (Properties.Settings.Default.APR == "y") apr.Checked = true;
        }

        private void DmsSettings_Load(object sender, EventArgs e)
        {
            LoadSettings();

        }

        private void clientname_CheckedChanged(object sender, EventArgs e)
        {
            switch (clientname.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.ClientName = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.ClientName = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void manager_CheckedChanged(object sender, EventArgs e)
        {
            switch (manager.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Manager = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Manager = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void salesperson_CheckedChanged(object sender, EventArgs e)
        {
            switch (salesperson.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.SalesPerson = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.SalesPerson = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void stocknum_CheckedChanged(object sender, EventArgs e)
        {
            switch (stocknum.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Stock = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Stock = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void sellingprice_CheckedChanged(object sender, EventArgs e)
        {
            switch (sellingprice.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.SellingPrice = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.SellingPrice = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void tradeamount_CheckedChanged(object sender, EventArgs e)
        {
            switch (tradeamount.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.TradeAmount = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.TradeAmount = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void payoff_CheckedChanged(object sender, EventArgs e)
        {
            switch (payoff.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Payoff = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Payoff = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void payment_CheckedChanged(object sender, EventArgs e)
        {
            switch (payment.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Payment = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Payment = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void rebate_CheckedChanged(object sender, EventArgs e)
        {
            switch (rebate.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Rebate = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Rebate = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void daystopay_CheckedChanged(object sender, EventArgs e)
        {
            switch (daystopay.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.DaysToPay = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.DaysToPay = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void salestax_CheckedChanged(object sender, EventArgs e)
        {
            switch (salestax.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.SalesTax = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.SalesTax = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void fees_CheckedChanged(object sender, EventArgs e)
        {
            switch (fees.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Fees = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Fees = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void products_CheckedChanged(object sender, EventArgs e)
        {
            switch (products.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Products = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Products = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void blank_CheckedChanged(object sender, EventArgs e)
        {
            switch (blank.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.blank = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.blank = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void downpay_CheckedChanged(object sender, EventArgs e)
        {
            switch (downpay.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.Downpay = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.Downpay = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }

        private void apr_CheckedChanged(object sender, EventArgs e)
        {
            switch (apr.CheckState)
            {
                case CheckState.Checked:
                    Properties.Settings.Default.APR = "y";
                    Properties.Settings.Default.Save();
                    break;
                default:
                    Properties.Settings.Default.APR = "n";
                    Properties.Settings.Default.Save();
                    break;
            }
        }
    }
}
