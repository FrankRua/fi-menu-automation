﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Xml.Linq;
using Fiddler;
using MainMenu;
using MainMenu.Data;
using MainMenu.Data.DataProviders;
using MainMenu.Data.Utilities;
using MainMenu.Insurance;
using MainMenu.ObjectModel;
using MaterialSkin;
using MaterialSkin.Controls;
using Ninject;
using OptionSoft.Calculation;
using OptionSoft.Calculation.IInsurance;
using OptionSoft.Core.Net4;
using OptionSoft.Interfaces;
using OptionSoft.Licensing;
using TestStack.White.Configuration;
using TestStack.White.Factory;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;
using Application = TestStack.White.Application;
using Button = TestStack.White.UIItems.Button;
using CheckBox = TestStack.White.UIItems.CheckBox;
using ComboBox = TestStack.White.UIItems.ListBoxItems.ComboBox;
using Label = TestStack.White.UIItems.Label;
using License = OptionSoft.Licensing.License;
using OtherSetups = MainMenu.Data.DataProviders.OtherSetups;
using Panel = TestStack.White.UIItems.Panel;
using RadioButton = TestStack.White.UIItems.RadioButton;
using TextBox = TestStack.White.UIItems.TextBox;


namespace FI_Automation
{
    public partial class DMSTest : MaterialForm
    {
        public DMSTest()
        {
            InitializeComponent();
            SetupAutomation();
            CreateObjectGraph();
            CaptureConfiguration = new UrlCaptureConfiguration(); // Configure Fiddler.
            Loader.RunWorkerAsync();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Red700, Primary.Red700, Primary.Red700,
                Accent.Red700, TextShade.WHITE);

        }

        private UrlCaptureConfiguration CaptureConfiguration { get; }

        private const string connectionString =
            "datasource=127.0.0.1;port=3306;username=root;password=;database=reports;SslMode=none;";

        private const string Separator = ""; // For Serparting Requests
        private static readonly List<Tuple<string, int>> reportList = new List<Tuple<string, int>>();
        private static string XMLRequest; // Rating XML from us.
        private readonly XmlValues GetValues = new XmlValues(); // Grab XML Values for rating company

        private readonly List<string>
            OriginalValues = new List<string>(); // Original values for comparision to XML data.

        private readonly List<string> SentData = new List<string>(); // What the menu sent.
        private Application _application;
        private Window _mainWindow; // Get the Main window.

        private string filepath = string.Empty; // For current file path.
        private Form2 form2 = new Form2(); // XML Data View
        private int id; // Menu Process ID
        private bool loaded; // Is the menu loaded?
        private bool canPull = false;
        private string menu_title = string.Empty; // Because each menu title is the dealers name.
        private bool responseNeeded;
        private string responseString = string.Empty; // XML Response

        // Validation For Using External OptionSoft DLL's-----
        internal static void CreateObjectGraph()
        {
            // DIContainer.Instance.Bind<IFilePathProvider>().To<FilePathProvider>().InSingletonScope();
            DIContainer.Instance.Bind<IExpandoSettings>().To<ExpandoSettings>().InSingletonScope();
            DIContainer.Instance.Bind<IFilePathProvider>().To<FilePathProvider>().InSingletonScope();
            DIContainer.Instance.Bind<IDealerInfo>().To<DealerInfo>().InSingletonScope();
            DIContainer.Instance.Bind<IProgramSetup>().To<ProgramSetup>().InSingletonScope();
            DIContainer.Instance.Bind<IApplication>().To<_Application>().InSingletonScope();

            // Must load license /before/ using DIContainer.Instance.Get<IApplication>().License but /after/ 
            // DIContainer.Instance.Bind<IFilePathProvider>()...
            DIContainer.Instance.Bind<IAprSetups>().To<AprSetups>().InSingletonScope();
            DIContainer.Instance.Bind<IPaymentDefault>().To<PaymentDefault>().InSingletonScope();
            DIContainer.Instance.Bind<ILoadSaveDeal>().To<LoadSaveDeal>().InSingletonScope();

            // DIContainer.Instance.Bind<IDealerInfo>().To<Data.DataProviders.DealerInfo>().InSingletonScope();
            DIContainer.Instance.Bind<IGapTypes>().To<GapTypes>().InSingletonScope();
            DIContainer.Instance.Bind<ILenderSetups>().To<LenderSetups>().InSingletonScope();

            // DIContainer.Instance.Bind<IProgramSetup>().To<ProgramSetup>().InSingletonScope();
            DIContainer.Instance.Bind<IManagers>().To<Managers>().InSingletonScope();
            DIContainer.Instance.Bind<IOtherSetups>().To<OtherSetups>().InSingletonScope();
            DIContainer.Instance.Bind<IPackageProducts>().To<PackageProducts>().InSingletonScope();
            DIContainer.Instance.Bind<IPrintOptions>().To<PrintOptions>().InSingletonScope();
            DIContainer.Instance.Bind<IPackages>().To<Packages>().InSingletonScope();
            DIContainer.Instance.Bind<ISetupPrivileges>().To<SetupButtonPrivileges>().InSingletonScope();
            DIContainer.Instance.Bind<ITaxRebate>().To<TaxRebate>().InSingletonScope();
            DIContainer.Instance.Bind<IVSCOptions>().To<VSCOptions>().InSingletonScope();
            DIContainer.Instance.Bind<InsuranceInterface>().To<Insurances>().InSingletonScope();

            UserData.LoadEffectiveParentID();

            if (DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).IsDMSEnabled
                && DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).FeesFromService)
                DIContainer.Instance.Bind<IFeeSetups>().To<DmsFeeSetups>().InSingletonScope()
                    .WithConstructorArgument(UserData.EffectiveParentID);
            else DIContainer.Instance.Bind<IFeeSetups>().To<FeeSetups>().InSingletonScope();
            if (DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).IsDMSEnabled
                && DMSIntegration.GetDMSAbilities(UserData.EffectiveParentID).RebatesFromService)
                DIContainer.Instance.Bind<IRebateSetups>().To<DmsRebateSetups>().InSingletonScope()
                    .WithConstructorArgument(UserData.EffectiveParentID);
            else DIContainer.Instance.Bind<IRebateSetups>().To<RebateSetups>().InSingletonScope();
            LeaseRateSetups.Instance.Load();

            var datFilePath = DIContainer.Instance.Get<IFilePathProvider>().AddToWorkingPath("dealership.dat");

            try
            {
                DIContainer.Instance.Get<IApplication>().License = new License(datFilePath);
                DIContainer.Instance.Get<IApplication>().License.OpenLicense();
            }
            catch (LicenseFileNotFoundException)
            {
            }
        }


        private void SetupAutomation()
        {
            try
            {
                var processes = Process.GetProcessesByName("MainMenu");
                foreach (var process in processes)
                {
                    id = process.Id;
                    menu_title = process.MainWindowTitle;
                }

                CoreAppXmlConfiguration.Instance.BusyTimeout = 40000;

                // Attach to Menu based on ID from above.
                _application = Application.Attach(id);
                _mainWindow = _application.GetWindow(
                    SearchCriteria.ByText(menu_title),
                    InitializeOption.NoCache);
                loaded = true;
                GetFilePath();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not hook application, is the menu open? ");
                System.Windows.Forms.Application.Exit();
            }
        }

        /// <summary>
        ///     Starts up a new session to capture request data via the Fiddler DLL.
        /// </summary>
        private void Start()
        {
            if (FiddlerApplication.IsStarted())
            {
            }
            else
            {
                CaptureConfiguration.ProcessId = id;

                // CaptureConfiguration.CaptureDomain = 
                FiddlerApplication.AfterSessionComplete += Fiddler_AfterSession;
                FiddlerApplication.Startup(8888, true, true, true);
            }
        }

        /// <summary>
        ///     Stops all Fiddler Sessions. If you're getting proxy error's on your browser, use this method to stop the proxy
        ///     server.
        /// </summary>
        private void Stop()
        {
            FiddlerApplication.AfterSessionComplete -= Fiddler_AfterSession;

            if (FiddlerApplication.IsStarted())
                FiddlerApplication.Shutdown();
        }

        private void Fiddler_AfterSession(Session s)
        {
            // Ignore HTTPS connect requests
            if (s.RequestMethod == "CONNECT")
                return;

            if (CaptureConfiguration.ProcessId > 0)
                if (s.LocalProcessID != 0 && s.LocalProcessID != CaptureConfiguration.ProcessId)
                    return;

            if (!string.IsNullOrEmpty(CaptureConfiguration.CaptureDomain))
                if (s.hostname.ToLower() != CaptureConfiguration.CaptureDomain.Trim().ToLower())
                    return;

            if (CaptureConfiguration.IgnoreResources)
            {
                var url = s.fullUrl.ToLower();

                var extensions = CaptureConfiguration.ExtensionFilterExclusions;
                foreach (var ext in extensions)
                    if (url.Contains(ext))
                        return;

                var filters = CaptureConfiguration.UrlFilterExclusions;
                foreach (var urlFilter in filters)
                    if (url.Contains(urlFilter))
                        return;
            }

            if (s == null || s.oRequest == null || s.oRequest.headers == null)
                return;

            var headers = s.oRequest.headers.ToString();
            var reqBody = Encoding.UTF8.GetString(s.RequestBody);

            // if you wanted to capture the response
            var respHeaders = s.oResponse.headers.ToString();
            var respBody = Encoding.UTF8.GetString(s.ResponseBody);

            // replace the HTTP line to inject full URL
            var firstLine = s.RequestMethod + " " + s.fullUrl + " " + s.oRequest.headers.HTTPVersion;
            var at = headers.IndexOf("\r\n");
            if (at < 0)
                return;
            headers = firstLine + "\r\n" + headers.Substring(at + 1);

            var output = headers + "\r\n" + (!string.IsNullOrEmpty(reqBody) ? reqBody + "\r\n" : string.Empty)
                         + Separator + "\r\n\r\n";

            // So the UI doesn't lag.
            BeginInvoke(new Action<string>(text => { XMLRequest = text; }), output);

            if (responseNeeded) responseString = respBody;
        }


        public static bool InstallCertificate()
        {
            if (CertMaker.rootCertExists()) return true;
            if (!CertMaker.createRootCert())

                return false;

            if (!CertMaker.trustRootCert())
                return false;

            return true;
        }

        public static bool UninstallCertificate()
        {
            if (CertMaker.rootCertExists())
                if (!CertMaker.removeFiddlerGeneratedCerts(true))
                    return false;
            return true;
        }

        private static bool CheckIfNull(string dealID)
        {
            var _theDeal = new MenuDeal();
            var _DMSPull = DMSIntegration.GetDeal(_theDeal.GetParent(), dealID); // <- Current Menu Parent ID, Deal #

            if (_DMSPull != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        private Dictionary<string, string> ImportDmsValuesFromDeal()
        {
            var _theDeal = new MenuDeal();
            var _DMSPull =
                DMSIntegration.GetDeal(_theDeal.GetParent(), dealtxt.Text); // <- Current Menu Parent ID, Deal #

            if (_DMSPull == null)
            {

                MessageBox.Show("Cannot Pull Deal From DMS.");

            }
            else
            {
                canPull = true;

                var values = new Dictionary<string, string>
                {
                    {"Client Name", _DMSPull.TheDeal.Buyers.PrimaryBuyer.GetDisplayLastFirstMiddle(true)},
                    {"Stock Number", _DMSPull.TheDeal.Vehicle.StockNumber},
                    {"Selling Price", _DMSPull.TheDeal.SellingPrice.ToString()}
                };

                double? tradeamount = 0.00;
                double? payoff = 0;

                foreach (var item in _DMSPull.TheDeal.TradeIns)
                {
                    tradeamount += item.TradeAmount;
                    payoff += item.PayoffAmount;
                }

                values.Add("Trade Ins", tradeamount.ToString());
                values.Add("Payoff", payoff.ToString());
                values.Add("Downpayment", _DMSPull.TheDeal.CashDown.ToString());
                values.Add("Days To Pay",
                    string.IsNullOrEmpty(_DMSPull.TheDeal.DaysToPay.ToString())
                        ? "N/A"
                        : _DMSPull.TheDeal.DaysToPay.ToString());


                values.Add("Sales Tax", (_DMSPull.TheDeal.Taxes.Last().taxPercent * 100).ToString());

                double? totalTaxed = 0;
                double? totalUntaxed = 0;

                foreach (var item in _DMSPull.TheDeal.Fees)
                {
                    if (item.feeDescriptions != null && item.feeDescriptions.Count > 0)
                    {

                        values.Add((item.Name?? "null"),
                            (item?.Name ?? "null") + ("True" + ":" + item?.Amount ?? "null") + (":" + item?.Taxable ?? "null") +
                            (":" + item?.capitalizedFeeIndicator ?? "null") + (":" + item?.TaxRate ?? "null"));                  

                    }


                    if (item.Taxable == true)
                        totalTaxed += item.Amount;
                    else
                        totalUntaxed += item.Amount;
                }


                values.Add("Total Taxed", totalTaxed.ToString());
                values.Add("Total Untaxed", totalUntaxed.ToString());

                foreach (var item in _DMSPull.TheDeal.Products)
                {

                    if (values.ContainsKey(item?.Name ?? "null"))
                    {
                        values.Add(item?.Name ?? "Null" + " [Duplicate]", item.SellingPrice.ToString());
                    }


                    values.Add(item?.Name ?? "Null", item.SellingPrice.ToString());

                    values.Add("APR",
                        string.IsNullOrEmpty(_DMSPull.TheDeal.APR.ToString())
                            ? "N/A"
                            : _DMSPull.TheDeal.APR.ToString());
                    values.Add("Term",
                        string.IsNullOrEmpty(_DMSPull.TheDeal.Term.ToString())
                            ? "N/A"
                            : _DMSPull.TheDeal.Term.ToString());

                    values.Add("Payment",
                        string.IsNullOrEmpty(_DMSPull.TheDeal.otherFinanceInfo.paymentAmount.ToString())
                            ? _DMSPull.TheDeal.otherFinanceInfo.deferredDownPaymentAmount.ToString()
                            : _DMSPull.TheDeal.otherFinanceInfo.paymentAmount.ToString());

                    values.Add("Deal Type", _DMSPull.TheDeal.DealType.BaseType.ToString());
                    return values;
                }
            }

            return null;
            }
        


        private void GetFilePath()
        {
            var processes = Process.GetProcessesByName("MainMenu");
            foreach (var process in processes)
            {
                filepath = process.MainModule.FileName;
            }
        }

        /// <summary>
        ///     Checks to see if application is currently hooked into the Main Menu.
        /// </summary>
        private void CheckHook()
        {
            if (_mainWindow.Enabled) return;
            SetupAutomation();
        }

        /// <summary>
        ///     Strips away the unused namespaces from XML file.
        /// </summary>
        /// <param name="xmlDocument">The XML document.</param>
        /// <returns></returns>
        private static XElement StripNS(XElement xmlDocument)
        {
            var xmlDocumentWithoutNs = removeAllNamespaces(xmlDocument);
            return xmlDocumentWithoutNs;
        }

        private static XElement removeAllNamespaces(XElement xmlDocument)
        {
            var stripped = new XElement(xmlDocument.Name.LocalName);
            foreach (var attribute in xmlDocument.Attributes().Where(
                attribute => !attribute.IsNamespaceDeclaration && string.IsNullOrEmpty(attribute.Name.NamespaceName)))
                stripped.Add(new XAttribute(attribute.Name.LocalName, attribute.Value));
            if (!xmlDocument.HasElements)
            {
                stripped.Value = xmlDocument.Value;
                return stripped;
            }

            stripped.Add(xmlDocument.Elements().Select(el => StripNS(el)));
            return stripped;
        }

        private void DMSTest_Load(object sender, EventArgs e)
        {
            if (loaded) SendKeys.SendWait("{Enter}");
        }

        private void Loader_DoWork(object sender, DoWorkEventArgs e)
        {
            MessageBox.Show("Attempting to hook application, please wait...");
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            Stop();
            Start();
            var checkDMS = CheckIfNull(dealtxt.Text);
            if (checkDMS)
            {
                //If DMS Deal is valid -> If Form is Closed -> 
                if (form2.IsDisposed)
                {
                    // If Form not open -> Create new instance 
                    form2 = new Form2();
                    form2.Show();
                    form2.SendToBack();
                }
                else
                {
                    // If Form still open -> Close and make new instance.
                    form2.Close();
                    form2 = new Form2();
                    form2.Show();
                    form2.SendToBack();
                }

                runDMSQueryFromNewThread(materialCheckBox1.Checked);
            }
            else
            {
                MessageBox.Show("Cannot Pull Deal From DMS.");
            }
        }

        /// <summary>
        /// This should be used after you pull a DMS Deal. This will gather all fields and items from the menu and get them
        /// ready to compare to the DMS values.
        /// </summary>
        /// <param name="OnDMSPull">The on DMS pull.</param>
        private void MenuItemsAfterDms(Dictionary<string, string> OnDMSPull)
        {
            var DMSValuesInsideMenu = new Dictionary<string, string>();

            var nameMapping = new Dictionary<string, int>
            {
                {"prodOne", 1},
                {"prodTwo", 2},
                {"prodThree", 3},
                {"prodFour", 4},
                {"prodFive", 5},
                {"prodSix", 6},
                {"prodSeven", 7},
                {"prodEight", 8}
            };

            var checkMapping = new Dictionary<string, int>
            {
                {"prodBox1", 1},
                {"prodBox2", 2},
                {"prodBox3", 3},
                {"prodBox4", 4},
                {"prodBox5", 5},
                {"prodBox6", 6},
                {"prodBox7", 7},
                {"prodBox8", 8}
            };

            progressBar1.Invoke((MethodInvoker)delegate
           {
               progressBar1.Value += 5;
               status.Text = progressBar1.Value + "% " + " Gathering Menu Items...";
           });

            if (Properties.Settings.Default.ClientName == "y")
            {
                try
                {
                    var txtName = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtName")).Text;
                    DMSValuesInsideMenu.Add("Name", txtName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Manager == "y")
            {
                try
                {
                    var txtManager = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("txtManager"))
                        .SelectedItemText;
                    DMSValuesInsideMenu.Add("Manager", txtManager);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.SalesPerson == "y")
            {
                try
                {
                    var txtSalesMan = _mainWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("txtSalesMan"))
                        .SelectedItemText;
                    DMSValuesInsideMenu.Add("Associate", txtSalesMan);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Stock == "y")
            {
                try
                {
                    var txtStockNum = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtStockNum")).Text;
                    DMSValuesInsideMenu.Add("Stock Number", txtStockNum);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.SellingPrice == "y")
            {
                try
                {
                    var txtPrice = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtPrice")).Text;
                    DMSValuesInsideMenu.Add("Price", txtPrice);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Fees == "y")
            {
                try
                {
                    var taxedFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblTaxedFees")).Text;
                    DMSValuesInsideMenu.Add("Taxed Fees", taxedFees);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Fees == "y")
            {
                try
                {
                    var untaxedFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblUntaxedFees")).Text;
                    DMSValuesInsideMenu.Add("UnTaxed Fees", untaxedFees);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Fees == "y")
            {
                try
                {
                    var totalFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblTotalFees")).Text;
                    DMSValuesInsideMenu.Add("Total Fees", totalFees);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.TradeAmount == "y")
            {
                try
                {
                    var txtTrade = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtTrade")).Text;
                    DMSValuesInsideMenu.Add("Trade", txtTrade);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Payoff == "y")
            {
                try
                {
                    var txtPayoff = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtPayoff")).Text;
                    DMSValuesInsideMenu.Add("Payoff", txtPayoff);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.Downpay == "y")
            {
                try
                {
                    var txtDown = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtDown")).Text;
                    DMSValuesInsideMenu.Add("Downpayment", txtDown);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.blank == "y") //TODO: This
            {

                try
                {
                    var txtServiceTerm =
                        _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtServiceTerm")).Text;
                    DMSValuesInsideMenu.Add("ServiceTerm", txtServiceTerm);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.blank == "y") //TODO: This
            {
                try
                {
                    var txtServiceMiles = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtServiceMiles"))
                        .Text;
                    DMSValuesInsideMenu.Add("TermMiles", txtServiceMiles);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.blank == "y") // TODO: This
            {
                try
                {
                    var cbDeduct = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("1001")).Text;
                    DMSValuesInsideMenu.Add("Deduct", cbDeduct);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.SalesTax == "y")
            {
                try
                {
                    var txtSalesTax = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("txtSalesTax")).Text;
                    DMSValuesInsideMenu.Add("Sales Tax", txtSalesTax);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);


                }
            }

            if (Properties.Settings.Default.Payment == "y")
            {
                try
                {
                    var finalpay = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("finalPay")).Text;
                    DMSValuesInsideMenu.Add("Payment", finalpay); //placeholder
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.blank == "y") // TODO: This
            {
                try
                {
                    var term = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("termBox")).Text;
                    DMSValuesInsideMenu.Add("Term", term);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.APR == "y")
            {
                try
                {
                    var aprbox = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("aprBox")).Text;
                    DMSValuesInsideMenu.Add("APR", aprbox);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.blank == "y") //TODO: This
            {
                try
                {
                    var discount = _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("discountBox")).Text;
                    DMSValuesInsideMenu.Add("Discount", discount);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            if (Properties.Settings.Default.blank == "y") //TODO: This
            {
                try
                {
                    var quickpay = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("quickPay")).Text;
                    DMSValuesInsideMenu.Add("Pay", quickpay);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }
            }

            // Get Payment info ->
            // Not sure why but the first element is last. So like 2,3,4,5,1
            progressBar1.Invoke((MethodInvoker)delegate
           {
               progressBar1.Value += 5;
               status.Text = progressBar1.Value + "% " + " Gathering Products...";
           });

            if (Properties.Settings.Default.Products == "y")
            {
                var paymentPanel = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("panel2"));
                var paymentCheckBoxes = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.CheckBox))
                    .Cast<CheckBox>().ToList();

                var planNames = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
                var prices = paymentPanel.GetMultiple(SearchCriteria.ByControlType(ControlType.Edit)).Cast<TextBox>()
                    .ToList();

                var orderedNames = planNames.OrderBy(x => nameMapping[x.Id]).ToList();
                var orderedCheckboxs = paymentCheckBoxes.OrderBy(x => checkMapping[x.Id]).ToList();

                for (var i = 0; i < orderedNames.Count; i++)
                {

                    DMSValuesInsideMenu.Add("Product (" + orderedNames[i].Name + " )",
                        orderedNames[i].Name + ":" + prices[i].Text);

                }
            }



            if (Properties.Settings.Default.Fees == "y")
            {
                // Fees - Grab Fee Name, Price, Taxed, Upfront and Enabled --------------------->
                progressBar1.Invoke((MethodInvoker)delegate
               {
                   progressBar1.Value += 5;
                   status.Text = progressBar1.Value + "% " + " Gathering Fees...";
               });


                var itemFees = new List<string>();
                var itemCheckBox = new List<string>();
                var options = new List<string> { "Enabled", "Taxed", "Upfront" };

                var lblFees = _mainWindow.Get<Label>(SearchCriteria.ByAutomationId("lblFees"));
                SendKeys.SendWait("{TAB}"); // Needs this or the label won't click for some reason.
                lblFees.Click();

                var _feeWindow = _mainWindow.ModalWindow("Fees");
                var flpRows = _mainWindow.Get<Panel>(SearchCriteria.ByAutomationId("flpRows"));
                var fees = _feeWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
                var fees_original = _feeWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.Text)).ToList();
                var fee_checkbox = _feeWindow.GetMultiple(SearchCriteria.ByControlType(ControlType.CheckBox)).ToList();

                // Remove all default values.
                fees.RemoveAll(
                    s => s.Name.ToString() == "Enabled" || s.Name.ToString() == "Description"
                                                        || s.Name.ToString() == "Amount" || s.Name.ToString() == "Taxed"
                                                        || s.Name.ToString() == "Upfront");

                foreach (var item_name in fees)
                {
                    var price = _feeWindow.Get<TextBox>(SearchCriteria.ByText(item_name.Name)).Text;
                    if (!DMSValuesInsideMenu.ContainsKey(item_name.Name))
                    {
                        DMSValuesInsideMenu.Add(item_name.Name, item_name.Name + ":" + price);
                    }

                    itemFees.Add(item_name.Name + " / " + price); // CANNOT PUT THESE IN TWO ITEMS (PRICE / NAME)
                }


                // I tested this with 1,2,3,4,5,6 fees. The logic works, don't ask me how but it does.

                form2.Invoke((MethodInvoker)delegate
               {

                   foreach (var item in OnDMSPull)
                   {
                       form2.listView2.Items.Add(item.Key + ":" + item.Value);
                   }

                   var interation = 0;
                   var keeptrack = 0;

                   var checkList = new List<string>();
                   var currentitem = string.Empty;
                   foreach (CheckBox checkBox in fee_checkbox)
                   {
                       if (interation % 3 == 0)
                       {
                           currentitem = itemFees[keeptrack / 3];
                       }

                       if (interation == 3) interation = 0;
                       checkList.Add(/*options[interation] + ":" + */ checkBox.Checked.ToString());
                       if (checkList.Count == 3)
                       {
                           form2.listView1.Items.Add(currentitem + ":" + checkList[0] + ":" + checkList[1] + ":" + checkList[2]);
                           checkList.Clear();
                       }

                       interation++;
                       keeptrack++;

                   }
               });
            }

            progressBar1.Invoke((MethodInvoker)delegate
           {
               progressBar1.Value += 5;
               status.Text = progressBar1.Value + "% " + "Items complied into view, complete..";
           });

        }

        private void runDMSQueryFromNewThread(bool notifyWhenComplete)
        {
            //Reset all values
            canPull = false;
            dealInfo.Clear();
            progressBar1.Value = 0;
            status.Text = " 0% : Idle";


            var menuDMSValues = ImportDmsValuesFromDeal();


            new Thread(() =>
            {

                if (canPull != true)
                {
                    return;
                }

                dealInfo.Invoke((MethodInvoker) delegate
                {
                    dealInfo.Text = "Client Name: " + menuDMSValues["Client Name"] + Environment.NewLine +
                                    "Stock Number: " + menuDMSValues["Stock Number"] + Environment.NewLine +
                                    "Selling Price: $" + menuDMSValues["Selling Price"] + Environment.NewLine +
                                    "Total Taxed Amount: $" + menuDMSValues["Total Taxed"] +
                                    Environment.NewLine +
                                    "Total Untaxed Amount: $" + menuDMSValues["Total Untaxed"] +
                                    Environment.NewLine +
                                    "Payment: $" + menuDMSValues["Payment"] + Environment.NewLine;
                });

                progressBar1.Invoke((MethodInvoker) delegate
                {
                    progressBar1.Value += 10;
                    status.Text = progressBar1.Value + "% " + " Pulling deal from DMS...";
                });

                SetupAutomation();


                responseNeeded = true; // Tell it that we care about the response.
                var MenuMB = _mainWindow.MenuBar;
                var openDMS = MenuMB.MenuItem("File", "Open Deal", "DMS Retrieval");
                openDMS.Click();
                var DealNumberTextBox =
                    _mainWindow.Get<TextBox>(SearchCriteria.ByAutomationId("DealNumberTextBox"));
                DealNumberTextBox.Text = dealtxt.Text;
                var okButton = _mainWindow.Get<Button>(SearchCriteria.ByAutomationId("okButton"));

                okButton.Click();

                dms_check.Enabled = true; // To pull request.

                // If Product Window Pops Up - This is a NEW WINDOW. Set to the new window then RESET back to main window.
                try
                {
                    progressBar1.Invoke((MethodInvoker) delegate
                    {
                        progressBar1.Value += 40;
                        status.Text = progressBar1.Value + "% " + " Checking for Unknown Products...";
                    });
                    _mainWindow = _application.GetWindow(
                        SearchCriteria.ByText("Loading Deal"), InitializeOption.NoCache);
                    using (var _productWindow = _mainWindow.ModalWindow("Unknown Product Found"))
                    {
                        _productWindow.Get<Button>(SearchCriteria.ByAutomationId("btnDelete")).Click();
                    }

                    _mainWindow =
                        _application.GetWindow(SearchCriteria.ByText(menu_title), InitializeOption.NoCache);
                    progressBar1.Invoke((MethodInvoker) delegate
                    {
                        progressBar1.Value += 10;
                        status.Text = progressBar1.Value + "% " + " Processing Deal Objects...";
                    });
                }
                catch (Exception ex)
                {
                    progressBar1.Invoke((MethodInvoker) delegate
                    {
                        progressBar1.Value += 10;
                        status.Text = progressBar1.Value + "% " + " Processing Deal Objects...";
                    });
                }

                progressBar1.Invoke((MethodInvoker) delegate
                {
                    progressBar1.Value += 20;
                    status.Text = progressBar1.Value + "% " + " Saving XML for data collection...";
                });

                File.WriteAllText(Environment.CurrentDirectory + @"\response.xml", responseString,
                    Encoding.UTF8); // Write Raw Request to file
                var data = StripNS(
                        XElement.Parse(File.ReadAllText(Environment.CurrentDirectory + @"\response.xml")))
                    .ToString();
                File.WriteAllText(Environment.CurrentDirectory + @"\response.xml", data,
                    Encoding.UTF8); // Write Raw Request to file, stripped of namespaces.
                responseNeeded = false; // Reset Flag.
                SetupAutomation(); // Relink

                MenuItemsAfterDms(menuDMSValues);

                if (notifyWhenComplete)
                {
                    MessageBox.Show("Complete!");
   
                }
                else
                {

                }

            }).Start();
        }


        private void materialRaisedButton1_KeyDown(object sender, KeyEventArgs e)
        {


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DmsSettings settings = new DmsSettings();
            settings.Show();
        }

        private void dealtxt_KeyDown(object sender, KeyEventArgs e)
        {



        }

        private void dealtxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regex = new Regex(@"[^a-zA-Z0-9\s]");
            if (regex.IsMatch(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void DMSTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            var menuList = new Menu();
            menuList.Show();
        }
    }
}