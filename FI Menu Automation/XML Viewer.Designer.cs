﻿namespace FI_Automation
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.menu = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listView2 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txt_Search = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.useHTMLtable = new MaterialSkin.Controls.MaterialCheckBox();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.menu});
            this.listView1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.ForeColor = System.Drawing.Color.Black;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(0, 63);
            this.listView1.Name = "listView1";
            this.listView1.ShowItemToolTips = true;
            this.listView1.Size = new System.Drawing.Size(634, 621);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.listView1_DrawItem);
            this.listView1.Enter += new System.EventHandler(this.listView1_Enter);
            // 
            // menu
            // 
            this.menu.Text = "From Menu";
            this.menu.Width = 629;
            // 
            // listView2
            // 
            this.listView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listView2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView2.ForeColor = System.Drawing.Color.Black;
            this.listView2.GridLines = true;
            this.listView2.Location = new System.Drawing.Point(631, 63);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(612, 621);
            this.listView2.TabIndex = 1;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "From Request";
            this.columnHeader1.Width = 607;
            // 
            // txt_Search
            // 
            this.txt_Search.BackColor = System.Drawing.SystemColors.Control;
            this.txt_Search.Depth = 0;
            this.txt_Search.Hint = "";
            this.txt_Search.Location = new System.Drawing.Point(308, 697);
            this.txt_Search.MouseState = MaterialSkin.MouseState.HOVER;
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.SelectedText = "";
            this.txt_Search.SelectionLength = 0;
            this.txt_Search.SelectionStart = 0;
            this.txt_Search.Size = new System.Drawing.Size(923, 23);
            this.txt_Search.TabIndex = 4;
            this.txt_Search.UseSystemPasswordChar = false;
            this.txt_Search.Visible = false;
            // 
            // useHTMLtable
            // 
            this.useHTMLtable.AutoSize = true;
            this.useHTMLtable.Checked = true;
            this.useHTMLtable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useHTMLtable.Depth = 0;
            this.useHTMLtable.Enabled = false;
            this.useHTMLtable.Font = new System.Drawing.Font("Roboto", 10F);
            this.useHTMLtable.Location = new System.Drawing.Point(9, 694);
            this.useHTMLtable.Margin = new System.Windows.Forms.Padding(0);
            this.useHTMLtable.MouseLocation = new System.Drawing.Point(-1, -1);
            this.useHTMLtable.MouseState = MaterialSkin.MouseState.HOVER;
            this.useHTMLtable.Name = "useHTMLtable";
            this.useHTMLtable.Ripple = true;
            this.useHTMLtable.Size = new System.Drawing.Size(291, 30);
            this.useHTMLtable.TabIndex = 5;
            this.useHTMLtable.Text = "Generate HTML Table (Required for email)";
            this.useHTMLtable.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 729);
            this.Controls.Add(this.useHTMLtable);
            this.Controls.Add(this.txt_Search);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.listView1);
            this.KeyPreview = true;
            this.Name = "Form2";
            this.Text = "XML Data Viewer";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form2_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ColumnHeader menu;
        public System.Windows.Forms.ListView listView1;
        public System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private MaterialSkin.Controls.MaterialSingleLineTextField txt_Search;
        public MaterialSkin.Controls.MaterialCheckBox useHTMLtable;
    }
}