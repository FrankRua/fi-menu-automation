﻿namespace FI_Automation
{
    public class XmlValues
    {
        public string[] Premier =
        {
            "ContractDate", "BasePayAmount", "BasePayRate", "CashDown", "ClientType", "DaysToPay", "BaseType", "Name",
            "FIManager", "FinalPay", "LeaseAmountFinanced", "MSRP", "ResidualValue", "SalesAssociate",
            "SellPrice", "State", "Term", "TotalAmount", "DealerId", "RatesProvider", "Condition", "Mileage", "Price",
            "VIN"
        };


    }
}