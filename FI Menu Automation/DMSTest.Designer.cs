﻿namespace FI_Automation
{
    partial class DMSTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DMSTest));
            this.dealtxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.Loader = new System.ComponentModel.BackgroundWorker();
            this.dms_check = new System.Windows.Forms.Timer(this.components);
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.status = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.dealInfo = new System.Windows.Forms.TextBox();
            this.materialCheckBox1 = new MaterialSkin.Controls.MaterialCheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dealtxt
            // 
            this.dealtxt.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dealtxt.Depth = 0;
            this.dealtxt.Hint = "";
            this.dealtxt.Location = new System.Drawing.Point(171, 126);
            this.dealtxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.dealtxt.Name = "dealtxt";
            this.dealtxt.PasswordChar = '\0';
            this.dealtxt.SelectedText = "";
            this.dealtxt.SelectionLength = 0;
            this.dealtxt.SelectionStart = 0;
            this.dealtxt.Size = new System.Drawing.Size(416, 23);
            this.dealtxt.TabIndex = 80;
            this.dealtxt.UseSystemPasswordChar = false;
            this.dealtxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dealtxt_KeyDown);
            this.dealtxt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dealtxt_KeyPress);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(285, 419);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(168, 23);
            this.materialRaisedButton1.TabIndex = 79;
            this.materialRaisedButton1.Text = "Pull Deal";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            this.materialRaisedButton1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.materialRaisedButton1_KeyDown);
            // 
            // Loader
            // 
            this.Loader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Loader_DoWork);
            // 
            // dms_check
            // 
            this.dms_check.Interval = 1000;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(21, 454);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(642, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 81;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(337, 94);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(56, 19);
            this.materialLabel1.TabIndex = 82;
            this.materialLabel1.Text = "Deal #:";
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.BackColor = System.Drawing.Color.Transparent;
            this.status.Depth = 0;
            this.status.Font = new System.Drawing.Font("Roboto", 11F);
            this.status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.status.Location = new System.Drawing.Point(18, 435);
            this.status.MouseState = MaterialSkin.MouseState.HOVER;
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(64, 19);
            this.status.TabIndex = 83;
            this.status.Text = "0% : Idle";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(18, 212);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(115, 19);
            this.materialLabel2.TabIndex = 84;
            this.materialLabel2.Text = "Quick Deal Info:";
            // 
            // dealInfo
            // 
            this.dealInfo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dealInfo.Location = new System.Drawing.Point(22, 234);
            this.dealInfo.Multiline = true;
            this.dealInfo.Name = "dealInfo";
            this.dealInfo.ReadOnly = true;
            this.dealInfo.Size = new System.Drawing.Size(675, 170);
            this.dealInfo.TabIndex = 85;
            // 
            // materialCheckBox1
            // 
            this.materialCheckBox1.AutoSize = true;
            this.materialCheckBox1.Checked = true;
            this.materialCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.materialCheckBox1.Depth = 0;
            this.materialCheckBox1.Font = new System.Drawing.Font("Roboto", 10F);
            this.materialCheckBox1.Location = new System.Drawing.Point(285, 164);
            this.materialCheckBox1.Margin = new System.Windows.Forms.Padding(0);
            this.materialCheckBox1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialCheckBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialCheckBox1.Name = "materialCheckBox1";
            this.materialCheckBox1.Ripple = true;
            this.materialCheckBox1.Size = new System.Drawing.Size(168, 30);
            this.materialCheckBox1.TabIndex = 86;
            this.materialCheckBox1.Text = "Notify When Complete";
            this.materialCheckBox1.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(669, 454);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 23);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 87;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // DMSTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 488);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.materialCheckBox1);
            this.Controls.Add(this.dealInfo);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.status);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.dealtxt);
            this.Controls.Add(this.materialRaisedButton1);
            this.Name = "DMSTest";
            this.Text = "DMS Testing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DMSTest_FormClosing);
            this.Load += new System.EventHandler(this.DMSTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField dealtxt;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.ComponentModel.BackgroundWorker Loader;
        private System.Windows.Forms.Timer dms_check;
        private System.Windows.Forms.ProgressBar progressBar1;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel status;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.TextBox dealInfo;
        private MaterialSkin.Controls.MaterialCheckBox materialCheckBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}