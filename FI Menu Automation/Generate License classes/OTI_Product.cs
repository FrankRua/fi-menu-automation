﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace FI_Automation.MonthlyLoadDAL
{
    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "OTI_Product", Namespace = "http://schemas.datacontract.org/2004/07/MonthlyLoadDAL")]
    [Serializable]

    public class OTI_Product : IExtensibleDataObject, INotifyPropertyChanged
    {
        [NonSerialized]
        private ExtensionDataObject extensionDataField;
        [OptionalField]
        private string DisplayNameField;
        [OptionalField]
        private int IDField;
        [OptionalField]
        private string NameField;
        [OptionalField]
        private string PrivateLabelField;
        [OptionalField]
        private string TypeField;

        [Browsable(false)]
        public ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [DataMember]
        public string DisplayName
        {
            get
            {
                return this.DisplayNameField;
            }
            set
            {
                if ((object)this.DisplayNameField == (object)value)
                    return;
                this.DisplayNameField = value;
                this.RaisePropertyChanged(nameof(DisplayName));
            }
        }

        [DataMember]
        public int ID
        {
            get
            {
                return this.IDField;
            }
            set
            {
                if (this.IDField.Equals(value))
                    return;
                this.IDField = value;
                this.RaisePropertyChanged(nameof(ID));
            }
        }

        [DataMember]
        public string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                if ((object)this.NameField == (object)value)
                    return;
                this.NameField = value;
                this.RaisePropertyChanged(nameof(Name));
            }
        }

        [DataMember]
        public string PrivateLabel
        {
            get
            {
                return this.PrivateLabelField;
            }
            set
            {
                if ((object)this.PrivateLabelField == (object)value)
                    return;
                this.PrivateLabelField = value;
                this.RaisePropertyChanged(nameof(PrivateLabel));
            }
        }

        [DataMember]
        public string Type
        {
            get
            {
                return this.TypeField;
            }
            set
            {
                if ((object)this.TypeField == (object)value)
                    return;
                this.TypeField = value;
                this.RaisePropertyChanged(nameof(Type));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            // ISSUE: reference to a compiler-generated field
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged((object)this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

