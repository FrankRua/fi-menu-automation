﻿
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace FI_Automation.MonthlyLoadDAL
{
    [DebuggerStepThrough]
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public class ServiceClient : ClientBase<Service>, Service
    {
        public ServiceClient()
        {
        }

        public ServiceClient(string endpointConfigurationName)
            : base(endpointConfigurationName)
        {
        }

        public ServiceClient(string endpointConfigurationName, string remoteAddress)
            : base(endpointConfigurationName, remoteAddress)
        {
        }

        public ServiceClient(string endpointConfigurationName, EndpointAddress remoteAddress)
            : base(endpointConfigurationName, remoteAddress)
        {
        }

        public ServiceClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
        }

        public Seat[] GetAllSeats()
        {
            return this.Channel.GetAllSeats();
        }

        public Seat CreateNewSeat(Seat s)
        {
            return this.Channel.CreateNewSeat(s);
        }

        public bool UpdateSeat(Seat s)
        {
            return this.Channel.UpdateSeat(s);
        }

        public string[] GetIntegrationTypes()
        {
            return this.Channel.GetIntegrationTypes();
        }

        public void AddIntegration(string parentCompanyId, string integrationType, string remoteId)
        {
            this.Channel.AddIntegration(parentCompanyId, integrationType, remoteId);
        }

        public OTI_Product[] GetAvailableOTIProducts()
        {
            return this.Channel.GetAvailableOTIProducts();
        }
    }
}
