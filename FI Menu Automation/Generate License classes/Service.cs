﻿using System.CodeDom.Compiler;
using System.ServiceModel;

namespace FI_Automation.MonthlyLoadDAL
{
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    [ServiceContract(ConfigurationName = "MonthlyLoadDAL.Service")]
    public interface Service
    {
        [OperationContract(Action = "http://tempuri.org/Service/GetAllSeats", ReplyAction = "http://tempuri.org/Service/GetAllSeatsResponse")]
        Seat[] GetAllSeats();

        [OperationContract(Action = "http://tempuri.org/Service/CreateNewSeat", ReplyAction = "http://tempuri.org/Service/CreateNewSeatResponse")]
        Seat CreateNewSeat(Seat s);

        [OperationContract(Action = "http://tempuri.org/Service/UpdateSeat", ReplyAction = "http://tempuri.org/Service/UpdateSeatResponse")]
        bool UpdateSeat(Seat s);

        [OperationContract(Action = "http://tempuri.org/Service/GetIntegrationTypes", ReplyAction = "http://tempuri.org/Service/GetIntegrationTypesResponse")]
        string[] GetIntegrationTypes();

        [OperationContract(Action = "http://tempuri.org/Service/AddIntegration", ReplyAction = "http://tempuri.org/Service/AddIntegrationResponse")]
        void AddIntegration(string parentCompanyId, string integrationType, string remoteId);

        [OperationContract(Action = "http://tempuri.org/Service/GetAvailableOTIProducts", ReplyAction = "http://tempuri.org/Service/GetAvailableOTIProductsResponse")]
        OTI_Product[] GetAvailableOTIProducts();
    }
}
