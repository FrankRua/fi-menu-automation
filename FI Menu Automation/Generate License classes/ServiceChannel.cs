﻿// Decompiled with JetBrains decompiler
// Type: MonthlyLoad.MonthlyLoadDAL.ServiceChannel
// Assembly: MonthlyLoad, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7165DC58-3364-4837-A987-616DC7053427
// Assembly location: C:\Users\frua\Desktop\MonthlyLoad.exe

using System;
using System.CodeDom.Compiler;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace FI_Automation.MonthlyLoadDAL
{
    [GeneratedCode("System.ServiceModel", "4.0.0.0")]
    public interface ServiceChannel : Service, IClientChannel, IContextChannel, IChannel, ICommunicationObject, IExtensibleObject<IContextChannel>, IDisposable
    {
    }
}
