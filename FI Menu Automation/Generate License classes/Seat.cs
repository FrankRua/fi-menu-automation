﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.Serialization;


namespace FI_Automation.MonthlyLoadDAL
{
    [DebuggerStepThrough]
    [GeneratedCode("System.Runtime.Serialization", "4.0.0.0")]
    [DataContract(Name = "Seat", Namespace = "http://schemas.datacontract.org/2004/07/MonthlyLoadDAL")]
    [Serializable]

    public class Seat : IExtensibleDataObject, INotifyPropertyChanged
    {
        [NonSerialized]
        private ExtensionDataObject extensionDataField;
        [OptionalField]
        private bool ActiveField;
        [OptionalField]
        private string AssignedVersionField;
        [OptionalField]
        private DateTime? DateCreatedField;
        [OptionalField]
        private string DealershipIDField;
        [OptionalField]
        private string DealershipNameField;
        [OptionalField]
        private bool OFACField;
        [OptionalField]
        private string ParentCompanyIDField;
        [OptionalField]
        private OTI_Product ProductField;
        [OptionalField]
        private string URLField;

        [Browsable(false)]
        public ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [DataMember]
        public bool Active
        {
            get
            {
                return this.ActiveField;
            }
            set
            {
                if (this.ActiveField.Equals(value))
                    return;
                this.ActiveField = value;
                this.RaisePropertyChanged(nameof(Active));
            }
        }

        [DataMember]
        public string AssignedVersion
        {
            get
            {
                return this.AssignedVersionField;
            }
            set
            {
                if ((object)this.AssignedVersionField == (object)value)
                    return;
                this.AssignedVersionField = value;
                this.RaisePropertyChanged(nameof(AssignedVersion));
            }
        }

        [DataMember]
        public DateTime? DateCreated
        {
            get
            {
                return this.DateCreatedField;
            }
            set
            {
                if (this.DateCreatedField.Equals((object)value))
                    return;
                this.DateCreatedField = value;
                this.RaisePropertyChanged(nameof(DateCreated));
            }
        }

        [DataMember]
        public string DealershipID
        {
            get
            {
                return this.DealershipIDField;
            }
            set
            {
                if ((object)this.DealershipIDField == (object)value)
                    return;
                this.DealershipIDField = value;
                this.RaisePropertyChanged(nameof(DealershipID));
            }
        }

        [DataMember]
        public string DealershipName
        {
            get
            {
                return this.DealershipNameField;
            }
            set
            {
                if ((object)this.DealershipNameField == (object)value)
                    return;
                this.DealershipNameField = value;
                this.RaisePropertyChanged(nameof(DealershipName));
            }
        }

        [DataMember]
        public bool OFAC
        {
            get
            {
                return this.OFACField;
            }
            set
            {
                if (this.OFACField.Equals(value))
                    return;
                this.OFACField = value;
                this.RaisePropertyChanged(nameof(OFAC));
            }
        }

        [DataMember]
        public string ParentCompanyID
        {
            get
            {
                return this.ParentCompanyIDField;
            }
            set
            {
                if ((object)this.ParentCompanyIDField == (object)value)
                    return;
                this.ParentCompanyIDField = value;
                this.RaisePropertyChanged(nameof(ParentCompanyID));
            }
        }

        [DataMember]
        public OTI_Product Product
        {
            get
            {
                return this.ProductField;
            }
            set
            {
                if (this.ProductField == value)
                    return;
                this.ProductField = value;
                this.RaisePropertyChanged(nameof(Product));
            }
        }

        [DataMember]
        public string URL
        {
            get
            {
                return this.URLField;
            }
            set
            {
                if ((object)this.URLField == (object)value)
                    return;
                this.URLField = value;
                this.RaisePropertyChanged(nameof(URL));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            // ISSUE: reference to a compiler-generated field
            PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if (propertyChanged == null)
                return;
            propertyChanged((object)this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

