﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FI_Automation.MonthlyLoadDAL;
using FI_Automation.Properties;
using MaterialSkin;
using MaterialSkin.Controls;
using OptionSoft.Core;
using OptionSoft.Core.Helpers;
using License = OptionSoft.Licensing.License;

namespace FI_Automation
{
    public partial class Menu : MaterialForm
    {
        public Menu()
        {
            InitializeComponent();
         

        }

        public List<Seat> seats;
        public List<Seat> filteredSeats;
        private readonly Service service = (Service)new ServiceClient();

        public void GetData()
        {
            try
            {
                ThreadHelper.Async((Action)(() => this.seats = this.filteredSeats = ((IEnumerable<Seat>)this.service.GetAllSeats()).ToList<Seat>()));
            }
            catch (Exception ex)
            {
                int num = (int)MessageBox.Show(ExceptionHelper.FormatException(ex));
            }

        }

        private void Menu_Load(object sender, EventArgs e)
        {

           GetData();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Main automationMenu = new Main();
            automationMenu.Show();
            (this).Hide();
            Enabled = false;

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            DMSTest dmsForm = new DMSTest();
            dmsForm.Show();
            (this).Hide();
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Seat seat = null;
            foreach (Seat s in filteredSeats)
            {
                if (s.ParentCompanyID == materialSingleLineTextField1.Text)
                {
                    seat = s;
                }
            }

            if (seat == null)
            {
                int num1 = (int)MessageBox.Show("No license file was created.\n Please restart the Monthly Load.");
            }
            else
            {
                new License(string.Format("c:\\installsforos\\datafiles\\{0}-{1}-{2}\\dealership.dat", (object)seat.DealershipName, (object)seat.ParentCompanyID, (object)seat.DealershipID), seat.DealershipName, seat.ParentCompanyID, seat.DealershipID).SaveLicense();
                Cursor.Current = Cursors.Default;
                int num2 = (int)MessageBox.Show("License file created.");
            }
        }
    }
}
