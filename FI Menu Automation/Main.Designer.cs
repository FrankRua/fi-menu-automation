﻿namespace FI_Automation
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.status = new System.Windows.Forms.Label();
            this.ratestat = new System.Windows.Forms.Label();
            this.checkBox1 = new MaterialSkin.Controls.MaterialCheckBox();
            this.newRadio = new MaterialSkin.Controls.MaterialRadioButton();
            this.oldRadio = new MaterialSkin.Controls.MaterialRadioButton();
            this.button1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.OpenMenuButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.admin_context = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.startPacketSniffingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopPacketSniffingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uninstallCertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.installVertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reHookApplicationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fISettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OTIServerMenuStrip = new System.Windows.Forms.ToolStripMenuItem();
            this.RatingServerMenuStrip = new MaterialSkin.Controls.MaterialToolStripMenuItem();
            this.showXMLTESTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grabItemsTESTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateTESTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.orangeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purpleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yellowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dms_check = new System.Windows.Forms.Timer(this.components);
            this.addProvider = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.addButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.odomttx = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.vintxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.daystopaytxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.Rebatetxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.downpaytxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.Payofftxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.TradeIntxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.SellingPricetxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.ClientNametxt = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Rating = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ClientName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.dealTypetxt = new MaterialSkin.Controls.MaterialLabel();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.admin_context.SuspendLayout();
            this.SuspendLayout();
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.BackColor = System.Drawing.Color.Transparent;
            this.status.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.ForeColor = System.Drawing.Color.White;
            this.status.Location = new System.Drawing.Point(1068, 848);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(23, 18);
            this.status.TabIndex = 10;
            this.status.Text = "---";
            // 
            // ratestat
            // 
            this.ratestat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ratestat.AutoSize = true;
            this.ratestat.BackColor = System.Drawing.Color.Transparent;
            this.ratestat.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ratestat.ForeColor = System.Drawing.Color.Crimson;
            this.ratestat.Location = new System.Drawing.Point(12, 756);
            this.ratestat.Name = "ratestat";
            this.ratestat.Size = new System.Drawing.Size(0, 24);
            this.ratestat.TabIndex = 33;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Depth = 0;
            this.checkBox1.Enabled = false;
            this.checkBox1.Font = new System.Drawing.Font("Roboto", 10F);
            this.checkBox1.Location = new System.Drawing.Point(736, 514);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(0);
            this.checkBox1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.checkBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Ripple = true;
            this.checkBox1.Size = new System.Drawing.Size(139, 30);
            this.checkBox1.TabIndex = 64;
            this.checkBox1.Text = "Use Random Rate";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // newRadio
            // 
            this.newRadio.AutoSize = true;
            this.newRadio.Checked = true;
            this.newRadio.Depth = 0;
            this.newRadio.Font = new System.Drawing.Font("Roboto", 10F);
            this.newRadio.Location = new System.Drawing.Point(891, 514);
            this.newRadio.Margin = new System.Windows.Forms.Padding(0);
            this.newRadio.MouseLocation = new System.Drawing.Point(-1, -1);
            this.newRadio.MouseState = MaterialSkin.MouseState.HOVER;
            this.newRadio.Name = "newRadio";
            this.newRadio.Ripple = true;
            this.newRadio.Size = new System.Drawing.Size(56, 30);
            this.newRadio.TabIndex = 65;
            this.newRadio.TabStop = true;
            this.newRadio.Text = "New";
            this.newRadio.UseVisualStyleBackColor = true;
            // 
            // oldRadio
            // 
            this.oldRadio.AutoSize = true;
            this.oldRadio.Depth = 0;
            this.oldRadio.Font = new System.Drawing.Font("Roboto", 10F);
            this.oldRadio.Location = new System.Drawing.Point(964, 514);
            this.oldRadio.Margin = new System.Windows.Forms.Padding(0);
            this.oldRadio.MouseLocation = new System.Drawing.Point(-1, -1);
            this.oldRadio.MouseState = MaterialSkin.MouseState.HOVER;
            this.oldRadio.Name = "oldRadio";
            this.oldRadio.Ripple = true;
            this.oldRadio.Size = new System.Drawing.Size(60, 30);
            this.oldRadio.TabIndex = 66;
            this.oldRadio.Text = "Used";
            this.oldRadio.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Depth = 0;
            this.button1.Location = new System.Drawing.Point(743, 709);
            this.button1.MouseState = MaterialSkin.MouseState.HOVER;
            this.button1.Name = "button1";
            this.button1.Primary = true;
            this.button1.Size = new System.Drawing.Size(281, 34);
            this.button1.TabIndex = 75;
            this.button1.Text = "Begin Testing";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // OpenMenuButton
            // 
            this.OpenMenuButton.Depth = 0;
            this.OpenMenuButton.Location = new System.Drawing.Point(693, 150);
            this.OpenMenuButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.OpenMenuButton.Name = "OpenMenuButton";
            this.OpenMenuButton.Primary = true;
            this.OpenMenuButton.Size = new System.Drawing.Size(137, 23);
            this.OpenMenuButton.TabIndex = 76;
            this.OpenMenuButton.Text = "Open Menu";
            this.OpenMenuButton.UseVisualStyleBackColor = true;
            this.OpenMenuButton.Visible = false;
            this.OpenMenuButton.Click += new System.EventHandler(this.OpenMenuButton_Click);
            // 
            // admin_context
            // 
            this.admin_context.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startPacketSniffingToolStripMenuItem,
            this.stopPacketSniffingToolStripMenuItem,
            this.uninstallCertToolStripMenuItem,
            this.installVertToolStripMenuItem,
            this.reHookApplicationToolStripMenuItem,
            this.fISettingsToolStripMenuItem,
            this.colorToolStripMenuItem});
            this.admin_context.Name = "admin_context";
            this.admin_context.Size = new System.Drawing.Size(189, 158);
            // 
            // startPacketSniffingToolStripMenuItem
            // 
            this.startPacketSniffingToolStripMenuItem.Name = "startPacketSniffingToolStripMenuItem";
            this.startPacketSniffingToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.startPacketSniffingToolStripMenuItem.Text = "Start - Packet Sniffing";
            this.startPacketSniffingToolStripMenuItem.Click += new System.EventHandler(this.startPacketSniffingToolStripMenuItem_Click);
            // 
            // stopPacketSniffingToolStripMenuItem
            // 
            this.stopPacketSniffingToolStripMenuItem.Name = "stopPacketSniffingToolStripMenuItem";
            this.stopPacketSniffingToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.stopPacketSniffingToolStripMenuItem.Text = "Stop - Packet Sniffing";
            this.stopPacketSniffingToolStripMenuItem.Visible = false;
            this.stopPacketSniffingToolStripMenuItem.Click += new System.EventHandler(this.stopPacketSniffingToolStripMenuItem_Click);
            // 
            // uninstallCertToolStripMenuItem
            // 
            this.uninstallCertToolStripMenuItem.ForeColor = System.Drawing.Color.Brown;
            this.uninstallCertToolStripMenuItem.Name = "uninstallCertToolStripMenuItem";
            this.uninstallCertToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.uninstallCertToolStripMenuItem.Text = "Uninstall Cert.";
            this.uninstallCertToolStripMenuItem.Visible = false;
            this.uninstallCertToolStripMenuItem.Click += new System.EventHandler(this.uninstallCertToolStripMenuItem_Click);
            // 
            // installVertToolStripMenuItem
            // 
            this.installVertToolStripMenuItem.ForeColor = System.Drawing.Color.DarkSeaGreen;
            this.installVertToolStripMenuItem.Name = "installVertToolStripMenuItem";
            this.installVertToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.installVertToolStripMenuItem.Text = "Install Cert.";
            this.installVertToolStripMenuItem.Visible = false;
            this.installVertToolStripMenuItem.Click += new System.EventHandler(this.installVertToolStripMenuItem_Click);
            // 
            // reHookApplicationToolStripMenuItem
            // 
            this.reHookApplicationToolStripMenuItem.ForeColor = System.Drawing.Color.Orange;
            this.reHookApplicationToolStripMenuItem.Name = "reHookApplicationToolStripMenuItem";
            this.reHookApplicationToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.reHookApplicationToolStripMenuItem.Text = "Re-Hook Application";
            this.reHookApplicationToolStripMenuItem.Click += new System.EventHandler(this.reHookApplicationToolStripMenuItem_Click);
            // 
            // fISettingsToolStripMenuItem
            // 
            this.fISettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OTIServerMenuStrip,
            this.RatingServerMenuStrip,
            this.showXMLTESTToolStripMenuItem,
            this.grabItemsTESTToolStripMenuItem,
            this.dateTESTToolStripMenuItem});
            this.fISettingsToolStripMenuItem.Name = "fISettingsToolStripMenuItem";
            this.fISettingsToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.fISettingsToolStripMenuItem.Text = "FI Settings";
            // 
            // OTIServerMenuStrip
            // 
            this.OTIServerMenuStrip.Name = "OTIServerMenuStrip";
            this.OTIServerMenuStrip.Size = new System.Drawing.Size(226, 22);
            this.OTIServerMenuStrip.Text = "OTI Server QA Mode: True";
            this.OTIServerMenuStrip.Click += new System.EventHandler(this.serverQAModeToolStripMenuItem_Click);
            // 
            // RatingServerMenuStrip
            // 
            this.RatingServerMenuStrip.AutoSize = false;
            this.RatingServerMenuStrip.Name = "RatingServerMenuStrip";
            this.RatingServerMenuStrip.Size = new System.Drawing.Size(226, 22);
            this.RatingServerMenuStrip.Text = "Rating Server QA Mode: True";
            this.RatingServerMenuStrip.Click += new System.EventHandler(this.ratingServerQAModeToolStripMenuItem_Click);
            // 
            // showXMLTESTToolStripMenuItem
            // 
            this.showXMLTESTToolStripMenuItem.Name = "showXMLTESTToolStripMenuItem";
            this.showXMLTESTToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.showXMLTESTToolStripMenuItem.Text = "Show XML (TEST)";
            this.showXMLTESTToolStripMenuItem.Click += new System.EventHandler(this.showXMLTESTToolStripMenuItem_Click);
            // 
            // grabItemsTESTToolStripMenuItem
            // 
            this.grabItemsTESTToolStripMenuItem.Name = "grabItemsTESTToolStripMenuItem";
            this.grabItemsTESTToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.grabItemsTESTToolStripMenuItem.Text = "GrabItems(TEST)";
            this.grabItemsTESTToolStripMenuItem.Click += new System.EventHandler(this.grabItemsTESTToolStripMenuItem_Click);
            // 
            // dateTESTToolStripMenuItem
            // 
            this.dateTESTToolStripMenuItem.Name = "dateTESTToolStripMenuItem";
            this.dateTESTToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.dateTESTToolStripMenuItem.Text = "Date (TEST)";
            this.dateTESTToolStripMenuItem.Click += new System.EventHandler(this.dateTESTToolStripMenuItem_Click);
            // 
            // colorToolStripMenuItem
            // 
            this.colorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.redToolStripMenuItem,
            this.redToolStripMenuItem1,
            this.orangeToolStripMenuItem,
            this.purpleToolStripMenuItem,
            this.greenToolStripMenuItem,
            this.yellowToolStripMenuItem});
            this.colorToolStripMenuItem.Name = "colorToolStripMenuItem";
            this.colorToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
            this.colorToolStripMenuItem.Text = "Color";
            // 
            // redToolStripMenuItem
            // 
            this.redToolStripMenuItem.Name = "redToolStripMenuItem";
            this.redToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.redToolStripMenuItem.Text = "Blue (Default)";
            this.redToolStripMenuItem.Click += new System.EventHandler(this.redToolStripMenuItem_Click);
            // 
            // redToolStripMenuItem1
            // 
            this.redToolStripMenuItem1.Name = "redToolStripMenuItem1";
            this.redToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.redToolStripMenuItem1.Text = "Red";
            this.redToolStripMenuItem1.Click += new System.EventHandler(this.redToolStripMenuItem1_Click);
            // 
            // orangeToolStripMenuItem
            // 
            this.orangeToolStripMenuItem.Name = "orangeToolStripMenuItem";
            this.orangeToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.orangeToolStripMenuItem.Text = "Orange";
            this.orangeToolStripMenuItem.Click += new System.EventHandler(this.orangeToolStripMenuItem_Click);
            // 
            // purpleToolStripMenuItem
            // 
            this.purpleToolStripMenuItem.Name = "purpleToolStripMenuItem";
            this.purpleToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.purpleToolStripMenuItem.Text = "Purple";
            this.purpleToolStripMenuItem.Click += new System.EventHandler(this.purpleToolStripMenuItem_Click);
            // 
            // greenToolStripMenuItem
            // 
            this.greenToolStripMenuItem.Name = "greenToolStripMenuItem";
            this.greenToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.greenToolStripMenuItem.Text = "Green";
            this.greenToolStripMenuItem.Click += new System.EventHandler(this.greenToolStripMenuItem_Click);
            // 
            // yellowToolStripMenuItem
            // 
            this.yellowToolStripMenuItem.Name = "yellowToolStripMenuItem";
            this.yellowToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.yellowToolStripMenuItem.Text = "Yellow";
            this.yellowToolStripMenuItem.Click += new System.EventHandler(this.yellowToolStripMenuItem_Click);
            // 
            // dms_check
            // 
            this.dms_check.Interval = 1000;
            this.dms_check.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // addProvider
            // 
            this.addProvider.Depth = 0;
            this.addProvider.Hint = "";
            this.addProvider.Location = new System.Drawing.Point(743, 516);
            this.addProvider.MouseState = MaterialSkin.MouseState.HOVER;
            this.addProvider.Name = "addProvider";
            this.addProvider.PasswordChar = '\0';
            this.addProvider.SelectedText = "";
            this.addProvider.SelectionLength = 0;
            this.addProvider.SelectionStart = 0;
            this.addProvider.Size = new System.Drawing.Size(169, 23);
            this.addProvider.TabIndex = 77;
            this.addProvider.UseSystemPasswordChar = false;
            this.addProvider.Visible = false;
            // 
            // addButton
            // 
            this.addButton.Depth = 0;
            this.addButton.Location = new System.Drawing.Point(929, 516);
            this.addButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.addButton.Name = "addButton";
            this.addButton.Primary = true;
            this.addButton.Size = new System.Drawing.Size(95, 23);
            this.addButton.TabIndex = 78;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Visible = false;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(16, 709);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(281, 34);
            this.materialRaisedButton1.TabIndex = 108;
            this.materialRaisedButton1.Text = "Add To Regression Queue";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(339, 698);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(55, 19);
            this.materialLabel10.TabIndex = 107;
            this.materialLabel10.Text = "Rating:";
            // 
            // odomttx
            // 
            this.odomttx.Depth = 0;
            this.odomttx.Hint = "";
            this.odomttx.Location = new System.Drawing.Point(343, 598);
            this.odomttx.MouseState = MaterialSkin.MouseState.HOVER;
            this.odomttx.Name = "odomttx";
            this.odomttx.PasswordChar = '\0';
            this.odomttx.SelectedText = "";
            this.odomttx.SelectionLength = 0;
            this.odomttx.SelectionStart = 0;
            this.odomttx.Size = new System.Drawing.Size(281, 23);
            this.odomttx.TabIndex = 105;
            this.odomttx.Text = "20000";
            this.odomttx.UseSystemPasswordChar = false;
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(339, 576);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(75, 19);
            this.materialLabel9.TabIndex = 106;
            this.materialLabel9.Text = "Odometer";
            // 
            // vintxt
            // 
            this.vintxt.Depth = 0;
            this.vintxt.Hint = "";
            this.vintxt.Location = new System.Drawing.Point(343, 536);
            this.vintxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.vintxt.Name = "vintxt";
            this.vintxt.PasswordChar = '\0';
            this.vintxt.SelectedText = "";
            this.vintxt.SelectionLength = 0;
            this.vintxt.SelectionStart = 0;
            this.vintxt.Size = new System.Drawing.Size(281, 23);
            this.vintxt.TabIndex = 103;
            this.vintxt.Text = "WMWSU3C57DT680244";
            this.vintxt.UseSystemPasswordChar = false;
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(339, 514);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(34, 19);
            this.materialLabel8.TabIndex = 104;
            this.materialLabel8.Text = "VIN";
            // 
            // daystopaytxt
            // 
            this.daystopaytxt.Depth = 0;
            this.daystopaytxt.Hint = "";
            this.daystopaytxt.Location = new System.Drawing.Point(179, 662);
            this.daystopaytxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.daystopaytxt.Name = "daystopaytxt";
            this.daystopaytxt.PasswordChar = '\0';
            this.daystopaytxt.SelectedText = "";
            this.daystopaytxt.SelectionLength = 0;
            this.daystopaytxt.SelectionStart = 0;
            this.daystopaytxt.Size = new System.Drawing.Size(118, 23);
            this.daystopaytxt.TabIndex = 102;
            this.daystopaytxt.Text = "45";
            this.daystopaytxt.UseSystemPasswordChar = false;
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(175, 640);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(88, 19);
            this.materialLabel6.TabIndex = 101;
            this.materialLabel6.Text = "Days to Pay";
            // 
            // Rebatetxt
            // 
            this.Rebatetxt.Depth = 0;
            this.Rebatetxt.Hint = "";
            this.Rebatetxt.Location = new System.Drawing.Point(16, 662);
            this.Rebatetxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.Rebatetxt.Name = "Rebatetxt";
            this.Rebatetxt.PasswordChar = '\0';
            this.Rebatetxt.SelectedText = "";
            this.Rebatetxt.SelectionLength = 0;
            this.Rebatetxt.SelectionStart = 0;
            this.Rebatetxt.Size = new System.Drawing.Size(118, 23);
            this.Rebatetxt.TabIndex = 99;
            this.Rebatetxt.Text = "0";
            this.Rebatetxt.UseSystemPasswordChar = false;
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(12, 640);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(55, 19);
            this.materialLabel7.TabIndex = 100;
            this.materialLabel7.Text = "Rebate";
            // 
            // downpaytxt
            // 
            this.downpaytxt.Depth = 0;
            this.downpaytxt.Hint = "";
            this.downpaytxt.Location = new System.Drawing.Point(499, 663);
            this.downpaytxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.downpaytxt.Name = "downpaytxt";
            this.downpaytxt.PasswordChar = '\0';
            this.downpaytxt.SelectedText = "";
            this.downpaytxt.SelectionLength = 0;
            this.downpaytxt.SelectionStart = 0;
            this.downpaytxt.Size = new System.Drawing.Size(125, 23);
            this.downpaytxt.TabIndex = 98;
            this.downpaytxt.Text = "0";
            this.downpaytxt.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(495, 640);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(104, 19);
            this.materialLabel4.TabIndex = 97;
            this.materialLabel4.Text = "Downpayment";
            // 
            // Payofftxt
            // 
            this.Payofftxt.Depth = 0;
            this.Payofftxt.Hint = "";
            this.Payofftxt.Location = new System.Drawing.Point(179, 598);
            this.Payofftxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.Payofftxt.Name = "Payofftxt";
            this.Payofftxt.PasswordChar = '\0';
            this.Payofftxt.SelectedText = "";
            this.Payofftxt.SelectionLength = 0;
            this.Payofftxt.SelectionStart = 0;
            this.Payofftxt.Size = new System.Drawing.Size(118, 23);
            this.Payofftxt.TabIndex = 95;
            this.Payofftxt.Text = "0";
            this.Payofftxt.UseSystemPasswordChar = false;
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(175, 577);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(52, 19);
            this.materialLabel5.TabIndex = 96;
            this.materialLabel5.Text = "Payoff";
            // 
            // TradeIntxt
            // 
            this.TradeIntxt.Depth = 0;
            this.TradeIntxt.Hint = "";
            this.TradeIntxt.Location = new System.Drawing.Point(343, 662);
            this.TradeIntxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.TradeIntxt.Name = "TradeIntxt";
            this.TradeIntxt.PasswordChar = '\0';
            this.TradeIntxt.SelectedText = "";
            this.TradeIntxt.SelectionLength = 0;
            this.TradeIntxt.SelectionStart = 0;
            this.TradeIntxt.Size = new System.Drawing.Size(125, 23);
            this.TradeIntxt.TabIndex = 94;
            this.TradeIntxt.Text = "0";
            this.TradeIntxt.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(339, 641);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(63, 19);
            this.materialLabel3.TabIndex = 93;
            this.materialLabel3.Text = "Trade In";
            // 
            // SellingPricetxt
            // 
            this.SellingPricetxt.Depth = 0;
            this.SellingPricetxt.Hint = "";
            this.SellingPricetxt.Location = new System.Drawing.Point(16, 598);
            this.SellingPricetxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.SellingPricetxt.Name = "SellingPricetxt";
            this.SellingPricetxt.PasswordChar = '\0';
            this.SellingPricetxt.SelectedText = "";
            this.SellingPricetxt.SelectionLength = 0;
            this.SellingPricetxt.SelectionStart = 0;
            this.SellingPricetxt.Size = new System.Drawing.Size(118, 23);
            this.SellingPricetxt.TabIndex = 91;
            this.SellingPricetxt.Text = "10000";
            this.SellingPricetxt.UseSystemPasswordChar = false;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(12, 576);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(92, 19);
            this.materialLabel11.TabIndex = 92;
            this.materialLabel11.Text = "Selling Price";
            // 
            // ClientNametxt
            // 
            this.ClientNametxt.Depth = 0;
            this.ClientNametxt.Hint = "";
            this.ClientNametxt.Location = new System.Drawing.Point(16, 536);
            this.ClientNametxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.ClientNametxt.Name = "ClientNametxt";
            this.ClientNametxt.PasswordChar = '\0';
            this.ClientNametxt.SelectedText = "";
            this.ClientNametxt.SelectionLength = 0;
            this.ClientNametxt.SelectionStart = 0;
            this.ClientNametxt.Size = new System.Drawing.Size(281, 23);
            this.ClientNametxt.TabIndex = 89;
            this.ClientNametxt.Text = "Test";
            this.ClientNametxt.UseSystemPasswordChar = false;
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.BackColor = System.Drawing.Color.Transparent;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(12, 514);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(92, 19);
            this.materialLabel12.TabIndex = 90;
            this.materialLabel12.Text = "Client Name";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(343, 722);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(281, 21);
            this.comboBox1.TabIndex = 88;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(12, 484);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(103, 19);
            this.materialLabel2.TabIndex = 87;
            this.materialLabel2.Text = "Add to Queue:";
            // 
            // listView1
            // 
            this.listView1.AllowColumnReorder = true;
            this.listView1.AllowDrop = true;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Rating,
            this.ClientName,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(16, 104);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1169, 357);
            this.listView1.TabIndex = 86;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Rating
            // 
            this.Rating.Text = "Rating";
            this.Rating.Width = 212;
            // 
            // ClientName
            // 
            this.ClientName.Text = "Client Name";
            this.ClientName.Width = 133;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Selling Price";
            this.columnHeader1.Width = 108;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Trade In";
            this.columnHeader2.Width = 113;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Payoff";
            this.columnHeader3.Width = 86;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Downpayment";
            this.columnHeader4.Width = 98;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Rebate";
            this.columnHeader5.Width = 75;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Days To Pay";
            this.columnHeader6.Width = 98;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "VIN";
            this.columnHeader7.Width = 123;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Odometer";
            this.columnHeader8.Width = 113;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(12, 80);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(55, 19);
            this.materialLabel1.TabIndex = 85;
            this.materialLabel1.Text = "Queue:";
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(738, 484);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(104, 19);
            this.materialLabel13.TabIndex = 109;
            this.materialLabel13.Text = "Control Panel:";
            // 
            // dealTypetxt
            // 
            this.dealTypetxt.AutoSize = true;
            this.dealTypetxt.BackColor = System.Drawing.Color.Transparent;
            this.dealTypetxt.Depth = 0;
            this.dealTypetxt.Font = new System.Drawing.Font("Roboto", 11F);
            this.dealTypetxt.ForeColor = System.Drawing.Color.Black;
            this.dealTypetxt.Location = new System.Drawing.Point(1119, 80);
            this.dealTypetxt.MouseState = MaterialSkin.MouseState.HOVER;
            this.dealTypetxt.Name = "dealTypetxt";
            this.dealTypetxt.Size = new System.Drawing.Size(66, 19);
            this.dealTypetxt.TabIndex = 110;
            this.dealTypetxt.Text = "New Car";
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(742, 679);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(133, 24);
            this.materialRaisedButton2.TabIndex = 111;
            this.materialRaisedButton2.Text = "Add Rating";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // materialRaisedButton3
            // 
            this.materialRaisedButton3.Depth = 0;
            this.materialRaisedButton3.Location = new System.Drawing.Point(891, 679);
            this.materialRaisedButton3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton3.Name = "materialRaisedButton3";
            this.materialRaisedButton3.Primary = true;
            this.materialRaisedButton3.Size = new System.Drawing.Size(132, 24);
            this.materialRaisedButton3.TabIndex = 112;
            this.materialRaisedButton3.Text = "Edit Rating List";
            this.materialRaisedButton3.UseVisualStyleBackColor = true;
            this.materialRaisedButton3.Click += new System.EventHandler(this.materialRaisedButton3_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 789);
            this.ContextMenuStrip = this.admin_context;
            this.Controls.Add(this.materialRaisedButton3);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.dealTypetxt);
            this.Controls.Add(this.materialLabel13);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.odomttx);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.vintxt);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.daystopaytxt);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.Rebatetxt);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.downpaytxt);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.Payofftxt);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.TradeIntxt);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.SellingPricetxt);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.ClientNametxt);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.OpenMenuButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.status);
            this.Controls.Add(this.ratestat);
            this.Controls.Add(this.oldRadio);
            this.Controls.Add(this.newRadio);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.addProvider);
            this.Name = "Main";
            this.Text = "Rating Testing";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.admin_context.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.Label ratestat;
        private MaterialSkin.Controls.MaterialCheckBox checkBox1;
        private MaterialSkin.Controls.MaterialRadioButton newRadio;
        private MaterialSkin.Controls.MaterialRadioButton oldRadio;
        private MaterialSkin.Controls.MaterialRaisedButton button1;
        private MaterialSkin.Controls.MaterialRaisedButton OpenMenuButton;
        private System.Windows.Forms.ContextMenuStrip admin_context;
        private System.Windows.Forms.ToolStripMenuItem startPacketSniffingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopPacketSniffingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uninstallCertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem installVertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reHookApplicationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fISettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OTIServerMenuStrip;
        private MaterialSkin.Controls.MaterialToolStripMenuItem RatingServerMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem colorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem orangeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purpleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yellowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showXMLTESTToolStripMenuItem;
        private System.Windows.Forms.Timer dms_check;
        private System.Windows.Forms.ToolStripMenuItem grabItemsTESTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem dateTESTToolStripMenuItem;
        private MaterialSkin.Controls.MaterialSingleLineTextField addProvider;
        private MaterialSkin.Controls.MaterialRaisedButton addButton;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialSingleLineTextField odomttx;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialSingleLineTextField vintxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialSingleLineTextField daystopaytxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialSingleLineTextField Rebatetxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialSingleLineTextField downpaytxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField Payofftxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialSingleLineTextField TradeIntxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialSingleLineTextField SellingPricetxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialSingleLineTextField ClientNametxt;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private System.Windows.Forms.ComboBox comboBox1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Rating;
        private System.Windows.Forms.ColumnHeader ClientName;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialLabel dealTypetxt;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton3;
    }
}

