﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin.Controls;
using MaterialSkin;

namespace FI_Automation
{
    public partial class Form2 : MaterialForm
    {
        public Form2()
        {
            InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Red700, Primary.Red700, Primary.Red700, Accent.Red700, TextShade.WHITE);
        }

        bool _setList = false;
        readonly Dictionary<string, string> _keyValuePairs = new Dictionary<string, string>();

        private void listView1_DrawItem(object sender, DrawListViewItemEventArgs e)
        {


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {
  
           
            if (_setList == false)
            {
                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    _keyValuePairs.Add(listView1.Items[i].Text, listView1.Items[i].BackColor.Name);
                }
            }
            _setList = true;

            var resultsRevised = _keyValuePairs.Where(s=> s.Key.ToLower().Contains(txt_Search.Text.ToLower())).ToList();
            listView1.Items.Clear();
            for (int y = 0; y < resultsRevised.Count; y++)
            {
                listView1.Items.Add(resultsRevised[y].Key).BackColor = Color.FromName(resultsRevised[y].Value);

            }

        }

        private void txt_Search_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void listView1_Enter(object sender, EventArgs e)
        {

        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Modifiers == Keys.Control && e.KeyCode == Keys.F)
            {
                txt_Search.Visible = true;
            }
        }
    }
}
